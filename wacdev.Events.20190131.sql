﻿CREATE TABLE `event_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

INSERT INTO `wacstaging`.`event_type`
(description)
VALUES
('training');

CREATE TABLE `wacstaging`.`event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_area_id` int(11) NOT NULL,
  `event_type_id` int(11) NOT NULL,
  `event_date` datetime NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `location` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `event_event_type_idx` (`event_type_id`),
  KEY `event_club_area_idx` (`club_area_id`),
  CONSTRAINT `event_club_area` FOREIGN KEY (`club_area_id`) REFERENCES `club_area` (`id`),
  CONSTRAINT `event_event_type` FOREIGN KEY (`event_type_id`) REFERENCES `event_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4;

INSERT INTO `wacstaging`.`role`
(`club_area_id`,
`name`)
VALUES
(1,
'Training Diary Administrator');
