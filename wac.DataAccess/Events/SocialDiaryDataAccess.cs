﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.DataAccess.Events
{
    public class SocialDiaryDataAccess : ISocialDiaryDataAccess
    {
        private readonly Settings _settings;
        private readonly IDateTimeProvider _dateTimeProvider;

        public SocialDiaryDataAccess(ISettingsProvider settingsProvider, IDateTimeProvider dateTimeProvider)
        {
            _settings = settingsProvider.Get();
            _dateTimeProvider = dateTimeProvider;
        }

        public SocialEvent Create(SocialEvent socialEvent)
        {
            var commandText = "INSERT INTO social_event" +
                " (club_area_id, title, event_date, event_time, location, description, organiser, email, facebook)" +
                "  VALUES (@clubAreaId, @title, @eventDate, @eventTime, @location, @description, @organiser, @email, @facebook);";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubAreaId", socialEvent.ClubArea.Id);
                command.Parameters.AddWithValue("@title", socialEvent.Title);
                command.Parameters.AddWithValue("@eventDate", socialEvent.EventDate);
                command.Parameters.AddWithValue("@eventTime", socialEvent.EventTime);
                command.Parameters.AddWithValue("@location", socialEvent.Location);
                command.Parameters.AddWithValue("@description", socialEvent.Description);
                command.Parameters.AddWithValue("@organiser", socialEvent.Organiser);
                command.Parameters.AddWithValue("@email", socialEvent.Email);
                command.Parameters.AddWithValue("@facebook", socialEvent.Facebook);


                command.ExecuteNonQuery();

                socialEvent.Id = (int)command.LastInsertedId;

                return socialEvent;
            }
        }

        public bool Delete(int id)
        {
            var commandText = "DELETE FROM social_event" +
                " WHERE id = @id;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@id", id);

                var success = command.ExecuteNonQuery();

                return success == 1;
            }
        }

        public List<SocialEvent> Fetch(int clubAreaId, bool limit)
        {
            var commandText = "SELECT e.id, ca.name club_area_name, ca.description club_area_description," +
                " e.club_area_id, e.title, e.event_date, e.event_time, e.location, e.description, e.organiser, e.email, e.facebook" +
                " FROM social_event e" +
                " INNER JOIN club_area ca ON e.club_area_id = ca.id" +
                " WHERE e.club_area_id = @clubAreaId" +
                " AND e.event_date >= @now" +
                " ORDER BY e.event_date" +
                (limit ? " LIMIT @socialDiaryListSize" : string.Empty);

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubAreaId", clubAreaId);
                command.Parameters.AddWithValue("@now", _dateTimeProvider.GetDateOnly(_dateTimeProvider.GetUtcNow()));
                if (limit)
                {
                    command.Parameters.AddWithValue("@socialDiaryListSize", _settings.Database.SocialDiaryListSize);
                }

                MySqlDataReader reader = command.ExecuteReader();
                var socialDiary = buildSocialDiary(reader);
                reader.Close();

                return socialDiary;
            }
        }

        public SocialEvent Get(int id)
        {
            var commandText = "SELECT e.id, ca.name club_area_name, ca.description club_area_description," +
                " e.club_area_id, e.title, e.event_date, e.event_time, e.location, e.description, e.organiser, e.email, e.facebook" +
                " FROM social_event e" +
                " INNER JOIN club_area ca ON e.club_area_id = ca.id" +
                " WHERE e.id = @id";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@id", id);

                MySqlDataReader reader = command.ExecuteReader();
                var socialEvent = buildSocialEvent(reader);
                reader.Close();

                return socialEvent;
            }
        }

        public bool Update(SocialEvent socialEvent)
        {
            var commandText = "UPDATE social_event" +
                " SET" +
                " club_area_id = @clubAreaId," +
                " event_date = @eventDate," +
                " event_time = @eventTime," +
                " location = @location," +
                " description = @description," +
                " organiser = @organiser," +
                " email = @email," +
                " facebook = @facebook" +
                " WHERE id = @id;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@id", socialEvent.Id);
                command.Parameters.AddWithValue("@clubAreaId", socialEvent.ClubArea.Id);
                command.Parameters.AddWithValue("@eventDate", socialEvent.EventDate);
                command.Parameters.AddWithValue("@eventTime", socialEvent.EventTime);
                command.Parameters.AddWithValue("@location", socialEvent.Location);
                command.Parameters.AddWithValue("@description", socialEvent.Description);
                command.Parameters.AddWithValue("@organiser", socialEvent.Organiser);
                command.Parameters.AddWithValue("@email", socialEvent.Email);
                command.Parameters.AddWithValue("@facebook", socialEvent.Facebook);

                var rowsAffected = command.ExecuteNonQuery();
                return rowsAffected == 1;
            }
        }

        private static List<SocialEvent> buildSocialDiary(MySqlDataReader reader)
        {
            var socialDiary = new List<SocialEvent>();

            while (reader.Read())
            {
                var entry = new SocialEvent
                {
                    Id = Convert.ToInt32(reader["id"]),
                    ClubArea = new ClubArea
                    {
                        Id = Convert.ToInt32(reader["club_area_id"]),
                        Description = reader["club_area_description"].ToString(),
                        Name = reader["club_area_name"].ToString()
                    },
                    Description = reader["description"].ToString(),
                    EventTime = Convert.ToDateTime(reader["event_time"]),
                    EventDate = Convert.ToDateTime(reader["event_date"]),
                    Location = reader["location"].ToString(),
                    Email = reader["email"].ToString(),
                    Facebook = reader["facebook"].ToString(),
                    Organiser = reader["organiser"].ToString(),
                    Title = reader["title"].ToString()
                };
                socialDiary.Add(entry);
            }

            return socialDiary;
        }

        private SocialEvent buildSocialEvent(MySqlDataReader reader)
        {
            var socialEvent = new SocialEvent();

            while (reader.Read())
            {
                socialEvent.Id = Convert.ToInt32(reader["id"]);
                socialEvent.ClubArea = new ClubArea
                {
                    Id = Convert.ToInt32(reader["club_area_id"]),
                    Description = reader["club_area_description"].ToString(),
                    Name = reader["club_area_name"].ToString()
                };
                socialEvent.Description = reader["description"].ToString();
                socialEvent.EventTime = Convert.ToDateTime(reader["event_time"]);
                socialEvent.EventDate = Convert.ToDateTime(reader["event_date"]);
                socialEvent.Location = reader["location"].ToString();
                socialEvent.Email = reader["email"].ToString();
                socialEvent.Facebook = reader["facebook"].ToString();
                socialEvent.Organiser = reader["organiser"].ToString();
                socialEvent.Title = reader["title"].ToString();
            }

            return socialEvent;
        }
    }
}
