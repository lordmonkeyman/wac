﻿using System;
using System.Collections.Generic;
using wac.Types.Constants;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;
using MySql.Data.MySqlClient;

namespace wac.DataAccess.Events
{
    public class TrainingDiaryDataAccess : ITrainingDiaryDataAccess
    {
        private readonly Settings _settings;
        private readonly string _description;
        private readonly IDateTimeProvider _dateTimeProvider;

        public TrainingDiaryDataAccess(ISettingsProvider settingsProvider, IDateTimeProvider dateTimeProvider)
        {
            _settings = settingsProvider.Get();
            _description = EventTypeDescriptions.TRAINING;
            _dateTimeProvider = dateTimeProvider;
        }

        public List<Event> Fetch(int clubAreaId, DateTime weekCommencing)
        {
            var commandText = "SELECT e.id, e.club_area_id, ca.name club_area_name, ca.description club_area_description, e.event_type_id," +
                " et.description event_type_description, e.start_time, e.end_time, e.event_date," +
                " e.location, e.description" +
                " FROM event e" +
                " INNER JOIN event_type et ON e.event_type_id = et.id" +
                " INNER JOIN club_area ca ON e.club_area_id = ca.id" +
                " WHERE et.description = @description" +
                " AND e.club_area_id = @clubAreaId" +
                " AND e.event_date BETWEEN @weekCommencing AND @weekEnding";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubAreaId", clubAreaId);
                command.Parameters.AddWithValue("@description", _description);
                command.Parameters.AddWithValue("@weekCommencing", weekCommencing);
                command.Parameters.AddWithValue("@weekEnding", _dateTimeProvider.CalculateWeekEnding(weekCommencing));

                MySqlDataReader reader = command.ExecuteReader();
                var trainingDiary = buildTrainingDiary(reader);
                reader.Close();

                return trainingDiary;
            }
        }

        public List<Event> Fetch(DateTime weekCommencing)
        {
            var commandText = "SELECT e.id, e.club_area_id, ca.name club_area_name, ca.description club_area_description, e.event_type_id," +
                " et.description event_type_description, e.start_time, e.end_time, e.event_date," +
                " e.location, e.description" +
                " FROM event e" +
                " INNER JOIN event_type et ON e.event_type_id = et.id" +
                " INNER JOIN club_area ca ON e.club_area_id = ca.id" +
                " WHERE et.description = @description" +
                " AND e.event_date BETWEEN @weekCommencing AND @weekEnding" +
                " AND e.club_area_id = 1";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@description", _description);
                command.Parameters.AddWithValue("@weekCommencing", weekCommencing);
                command.Parameters.AddWithValue("@weekEnding", _dateTimeProvider.CalculateWeekEnding(weekCommencing));

                MySqlDataReader reader = command.ExecuteReader();
                var trainingDiary = buildTrainingDiary(reader);
                reader.Close();

                return trainingDiary;
            }
        }

        public Event Create(Event @event)
        {
            var commandText = "INSERT INTO event" +
                " (club_area_id, event_type_id, event_date, start_time, end_time, location, description)" +
                "  VALUES (@clubAreaId, 1, @eventDate, @startTime, @endTime, @location, @description);";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubAreaId", @event.ClubArea.Id);
                command.Parameters.AddWithValue("@eventDate", @event.EventDate);
                command.Parameters.AddWithValue("@startTime", @event.StartTime);
                command.Parameters.AddWithValue("@endTime", @event.EndTime);
                command.Parameters.AddWithValue("@location", @event.Location);
                command.Parameters.AddWithValue("@description", @event.Description);

                command.ExecuteNonQuery();

                @event.Id = (int)command.LastInsertedId;

                return @event;
            }
        }

        public bool Delete(int eventId)
        {
            throw new NotImplementedException();
        }

        public Event Get(int eventId)
        {
            throw new NotImplementedException();
        }

        public bool Update(Event @event)
        {
            var commandText = "UPDATE event" +
                " SET" +
                " club_area_id = @clubAreaId," +
                " event_date = @eventDate," +
                " start_time = @startTime," +
                " end_time = @endTime," +
                " location = @location," +
                " description = @description" +
                " WHERE id = @id;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@id", @event.Id);
                command.Parameters.AddWithValue("@clubAreaId", @event.ClubArea.Id);
                command.Parameters.AddWithValue("@eventDate", @event.EventDate);
                command.Parameters.AddWithValue("@startTime", @event.StartTime);
                command.Parameters.AddWithValue("@endTime", @event.EndTime);
                command.Parameters.AddWithValue("@location", @event.Location);
                command.Parameters.AddWithValue("@description", @event.Description);

                var rowsAffected = command.ExecuteNonQuery();
                return rowsAffected == 1;
            }
        }

        private static List<Event> buildTrainingDiary(MySqlDataReader reader)
        {
            var trainingDiary = new List<Event>();

            while (reader.Read())
            {
                var entry = new Event
                {
                    Id = Convert.ToInt32(reader["id"]),
                    ClubArea = new ClubArea
                    {
                        Id = Convert.ToInt32(reader["club_area_id"]),
                        Description = reader["club_area_description"].ToString(),
                        Name = reader["club_area_name"].ToString()
                    },
                    Description = reader["description"].ToString(),
                    EndTime = Convert.ToDateTime(reader["end_time"]),
                    StartTime = Convert.ToDateTime(reader["start_time"]),
                    EventDate = Convert.ToDateTime(reader["event_date"]),
                    Location = reader["location"].ToString(),
                    EventType = new EventType
                    {
                        Id = Convert.ToInt32(reader["event_type_id"]),
                        Description = reader["event_type_description"].ToString()
                    }
                };
                trainingDiary.Add(entry);
            }

            return trainingDiary;
        }
    }
}
