﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.DataAccess.Club
{
    public class ClubDataAccess : IClubDataAccess
    {
        private readonly Settings _settings;
        
        public ClubDataAccess(ISettingsProvider settingsProvider)
        {
            _settings = settingsProvider.Get();
        }

        public List<ClubArea> GetClubAreas()
        {
            var clubId = _settings.ClubId;

            var commandText = "SELECT ca.id as id," +
                " ca.name as name," +
                " ca.description as description" +
                " FROM club_area ca " +
                " INNER JOIN club c ON c.id = ca.club_id" +
                " WHERE c.id = @clubId";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubId", clubId);

                MySqlDataReader reader = command.ExecuteReader();
                var clubAreaList = new List<ClubArea>();

                while (reader.Read())
                {
                    var clubArea = new ClubArea
                    {
                        Id = Convert.ToInt32(reader["id"]),
                        Name = reader["name"].ToString(),
                        ClubId = clubId,
                        Description = reader["description"].ToString()
                    };
                    clubAreaList.Add(clubArea);
                }
                reader.Close();

                return clubAreaList;
            }
        }
    }
}
