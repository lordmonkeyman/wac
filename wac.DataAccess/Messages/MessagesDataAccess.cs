﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.DataAccess.Messages
{
    public class MessagesDataAccess : IMessagesDataAccess
    {
        private readonly Settings _settings;
        private MySqlConnection _connection;

        public MessagesDataAccess(ISettingsProvider settingsProvider)
        {
            _settings = settingsProvider.Get();

            _connection = new MySqlConnection
            {
                ConnectionString = _settings.Database.WacConnectionString
            };
            _connection.Open();
        }

        public void Create(Message message)
        {
            var commandText = "INSERT INTO messages (parent_id, title, text,user_id, created_datetime)"
                + " VALUES(@parent_id, @title, @text, @user_id, @created_datetime); ";
            var command = new MySqlCommand(commandText, _connection);

            command.Parameters.AddWithValue("@parent_id", message.ParentId);
            command.Parameters.AddWithValue("@title", message.Title);
            command.Parameters.AddWithValue("@text", message.Text);
            command.Parameters.AddWithValue("@user_id", message.User.Id);
            command.Parameters.AddWithValue("@created_datetime", message.CreatedDateTime);

            command.ExecuteNonQuery();
        }
        
        public List<Message> Fetch(int boardId)
        {
            var commandText = "SELECT m.id id,"
                + " m.parent_id parent_id,"
                + " m.title title,"
                + " m.`text` `text`,"
                + " m.user_id user_id,"
                + " u.first_name first_name,"
                + " u.second_name second_name,"
                + " m.created_datetime created_datetime"
                + " FROM messages m"
                + " INNER JOIN user u ON u.id = m.user_id"
                + " WHERE m.message_board_id = @messageBoardId";

            var command = new MySqlCommand(commandText, _connection);
            command.Parameters.AddWithValue("@messageBoardId", boardId);

            MySqlDataReader reader = command.ExecuteReader();
            var messageList = new List<Message>();

            while (reader.Read())
            {
                var message = new Message
                {
                    Id = Convert.ToInt32(reader["id"]),
                    Title = reader["title"].ToString(),
                    Text = reader["text"].ToString(),
                    ParentId = Convert.ToInt32(reader["parent_id"]),
                    BoardId = boardId,
                    User = new User
                    {
                        Id = Convert.ToInt32(reader["user_id"]),
                        FirstName = reader["first_name"].ToString(),
                        SecondName = reader["second_name"].ToString()
                    },
                    CreatedDateTime = Convert.ToDateTime(reader["created_datetime"])
                };
                messageList.Add(message);
            }
            reader.Close();

            return messageList;
        }
    }
}
