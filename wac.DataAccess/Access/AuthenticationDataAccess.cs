﻿using MySql.Data.MySqlClient;
using System;
using wac.Types;
using wac.Interfaces.Providers;
using wac.Interfaces.DataAccess.Access;
using System.Collections.Generic;

namespace wac.DataAccess.Access
{
    public class AuthenticationDataAccess : IAuthenticationDataAccess
    {
        private readonly Settings _settings;
        
        public AuthenticationDataAccess(ISettingsProvider settingsProvider)
        {
            _settings = settingsProvider.Get();
        }

        public User RequestToken(string email)
        {
            var commandText = "SELECT id, first_name, second_name, email FROM user WHERE email = @email;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@email", email);

                MySqlDataReader reader = command.ExecuteReader();
                var user = new User();

                while (reader.Read())
                {
                    user.Id = Convert.ToInt32(reader["id"]);
                    user.FirstName = reader["first_name"].ToString();
                    user.SecondName = reader["second_name"].ToString();
                    user.Email = reader["email"].ToString();
                }
                reader.Close();

                return user;
            }
        }

        private List<Role> GetUserRoles(int userId)
        {
            var commandText = "SELECT r.id, r.name" +
                " FROM user_roles ur" +
                " INNER JOIN role r ON ur.role_id = r.id" +
                " INNER JOIN club_area ca ON r.club_area_id = ca.id" +
                " WHERE ur.user_id = @userId" +
                " AND ca.club_id = @clubId";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@userId", userId);
                command.Parameters.AddWithValue("@clubId", _settings.ClubId);

                MySqlDataReader reader = command.ExecuteReader();
                var roles = new List<Role>();

                while (reader.Read())
                {
                    roles.Add(new Role
                    {
                        Id = Convert.ToInt32(reader["id"]),
                        Name = reader["name"].ToString()
                    });
                }
                reader.Close();

                return roles;
            }
        }

        public bool ValidateToken(string token, out User user)
        {
            var commandText = "SELECT id, first_name, second_name, email FROM user WHERE authentication_token = @token AND authentication_expiry > @now;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@token", token);
                command.Parameters.AddWithValue("@now", DateTime.Now);

                MySqlDataReader reader = command.ExecuteReader();
                user = new User();
                if (!reader.HasRows) return false;

                while (reader.Read())
                {
                    user.Id = Convert.ToInt32(reader["id"]);
                    user.FirstName = reader["first_name"].ToString();
                    user.SecondName = reader["second_name"].ToString();
                    user.Email = reader["email"].ToString();
                }
                reader.Close();

                user.Roles = GetUserRoles(user.Id);

                return true;
            }
        }

        public void UpdateUserWithTokenAndExpiry(User user)
        {
            var commandText = "UPDATE user SET authentication_token = @authenticationToken, authentication_expiry = @authenticationExpiry WHERE email = @email;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@email", user.Email);
                command.Parameters.AddWithValue("@authenticationToken", user.AuthenticationToken.ToString());
                command.Parameters.AddWithValue("@authenticationExpiry", user.AuthenticationExpiry);

                command.ExecuteNonQuery();
            }
        }
    }
}
