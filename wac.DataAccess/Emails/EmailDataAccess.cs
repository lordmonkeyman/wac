﻿using System;
using MySql.Data.MySqlClient;
using wac.Types;
using wac.Interfaces.DataAccess.Emails;
using wac.Interfaces.Providers;

namespace wac.DataAccess.Emails
{
    public class EmailDataAccess : IEmailDataAccess
    {
        private readonly Settings _settings;
        
        public EmailDataAccess(ISettingsProvider settingsProvider)
        {
            _settings = settingsProvider.Get();
        }

        public Email GetEmail(int id)
        {
            var commandText = "SELECT * FROM email WHERE id = @id;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@id", id);

                MySqlDataReader reader = command.ExecuteReader();
                var email = new Email();

                while (reader.Read())
                {
                    email.Id = Convert.ToInt32(reader["id"]);
                    email.ApiKey = reader["api_key"].ToString();
                    email.DomainName = reader["domain_name"].ToString();
                    email.From = reader["from"].ToString();
                    email.Subject = reader["subject"].ToString();
                    email.Text = reader["text"].ToString();
                }
                reader.Close();

                return email;
            }
        }
    }
}
