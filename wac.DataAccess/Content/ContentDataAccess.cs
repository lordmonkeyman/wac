﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.DataAccess.Content
{
    public class ContentDataAccess : IContentDataAccess
    {
        private readonly Settings _settings;
        
        public ContentDataAccess(ISettingsProvider settingsProvider)
        {
            _settings = settingsProvider.Get();
        }

        public bool DeleteContent(int id)
        {
            var commandText = "DELETE FROM content" +
                " WHERE id = @id;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@id", id);

                var rowsAffected = command.ExecuteNonQuery();
                return rowsAffected == 1;
            }
        }

        public bool UpdateContent(Types.Content content)
        {
            var commandText = "UPDATE content" +
                " SET club_area_id = @clubAreaId," +
                " title = @title, body = @body," +
                " publish_datetime = @publishDateTime," + 
                " active = @active, featured = @featured" +
                " WHERE id = @id;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@id", content.Id);
                command.Parameters.AddWithValue("@clubAreaId", content.ClubArea.Id);
                command.Parameters.AddWithValue("@title", content.Title);
                command.Parameters.AddWithValue("@body", content.Body);
                command.Parameters.AddWithValue("@publishDateTime", content.PublishDateTime);
                command.Parameters.AddWithValue("@active", content.Active);
                command.Parameters.AddWithValue("@featured", content.Featured);

                var rowsAffected = command.ExecuteNonQuery();
                return rowsAffected == 1;
            }
        }

        public Types.Content CreateContent(Types.Content content)
        {
            var commandText = "INSERT INTO content(" +
                "club_area_id, title, body, publish_datetime, active, featured)" +
                " VALUES(@clubAreaId, @title, @body, @publishDateTime, @active, @featured);";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubAreaId", content.ClubArea.Id);
                command.Parameters.AddWithValue("@title", content.Title);
                command.Parameters.AddWithValue("@body", content.Body);
                command.Parameters.AddWithValue("@publishDateTime", content.PublishDateTime);
                command.Parameters.AddWithValue("@active", content.Active);
                command.Parameters.AddWithValue("@featured", content.Featured);

                command.ExecuteNonQuery();

                content.Id = (int)command.LastInsertedId;

                return content;
            }
        }

        public List<Types.Content> FetchContent(DateTime fromDate, bool activeOnly, bool featuredOnly)
        {
            var contentPageSize = _settings.Database.ContentPageSize;
            var activeConstraint = activeOnly == true ? " AND c.active = @active" : "";
            var featuredConstraint = featuredOnly == true ? " AND c.featured = @featured" : "";

            var commandText = "SELECT c.id as id, ca.id as club_area_id, ca.name as club_area_name, c.title as title," +
                " c.body as body, c.publish_datetime as publish_datetime, c.active as active," +
                " c.featured as featured " +
                " FROM content c" +
                " INNER JOIN club_area ca ON c.club_area_id = ca.id" +
                " INNER JOIN club club ON ca.club_id = club.id" +
                " WHERE club.id = @clubId AND c.publish_datetime < @fromDate" +
                activeConstraint +
                featuredConstraint +
                " ORDER BY c.publish_datetime DESC" +
                " LIMIT @contentPageSize";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubId", _settings.ClubId);
                command.Parameters.AddWithValue("@fromDate", fromDate);
                command.Parameters.AddWithValue("@contentPageSize", contentPageSize);
                command.Parameters.AddWithValue("@active", 1);
                command.Parameters.AddWithValue("@featured", 1);

                MySqlDataReader reader = command.ExecuteReader();
                var contentList = buildContentList(reader);
                reader.Close();

                return contentList;
            }
        }

        public List<Types.Content> FetchContentForClubArea(int clubAreaId, DateTime fromDate, bool activeOnly, bool featuredOnly)
        {
            var contentPageSize = _settings.Database.ContentPageSize;
            var activeConstraint = activeOnly == true ? " AND c.active = @active" : "";
            var featuredConstraint = featuredOnly == true ? " AND c.featured = @featured" : "";

            var commandText = "SELECT c.id as id, ca.id as club_area_id, ca.name as club_area_name, c.title as title," +
                " c.body as body, c.publish_datetime as publish_datetime, c.active as active," +
                " c.featured as featured" +
                " FROM content c" +
                " INNER JOIN club_area ca ON c.club_area_id = ca.id" +
                " INNER JOIN club club ON ca.club_id = club.id" +
                " WHERE club.id = @clubId AND c.publish_datetime < @fromDate" +
                " AND ca.id = @clubAreaId" +
                activeConstraint +
                featuredConstraint +
                " ORDER BY c.publish_datetime DESC" +
                " LIMIT @contentPageSize";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubId", _settings.ClubId);
                command.Parameters.AddWithValue("@fromDate", fromDate);
                command.Parameters.AddWithValue("@clubAreaId", clubAreaId);
                command.Parameters.AddWithValue("@contentPageSize", contentPageSize);
                command.Parameters.AddWithValue("@active", 1);
                command.Parameters.AddWithValue("@featured", 1);

                MySqlDataReader reader = command.ExecuteReader();
                var contentList = buildContentList(reader);
                reader.Close();

                return contentList;
            }
        }

        public List<Types.Content> FetchFeaturedContent()
        {
            var commandText = "SELECT c.id as id," +
                 " c.title as title," +
                 " c.body as body," +
                 " c.active as active," +
                 " c.publish_datetime as publish_datetime," +
                 " ca.id as club_area_id, ca.name as club_area_name," +
                 " c.featured as featured" +
                 " FROM content c" +
                 " INNER JOIN club_area ca ON ca.id = c.club_area_id" +
                 " INNER JOIN club club ON club.id = ca.club_id" +
                 " WHERE active = 1" +
                 " AND club.id = @clubId" +
                 " AND c.featured = true" +
                 " ORDER BY c.publish_datetime DESC; ";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubId", _settings.ClubId);

                var reader = command.ExecuteReader();
                var contentList = buildContentList(reader);
                reader.Close();

                return contentList;
            }
        }

        public Types.Content GetContent(int id)
        {
            var commandText = "SELECT c.id as id, ca.id as club_area_id, ca.name as club_area_name, c.title as title," +
                " c.body as body, c.publish_datetime as publish_datetime, c.active as active," +
                " c.featured as featured" +
                " FROM content c" +
                " INNER JOIN club_area ca ON c.club_area_id = ca.id" +
                " INNER JOIN club club ON ca.club_id = club.id" +
                " WHERE c.id = @Id;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@Id", id);
                MySqlDataReader reader = command.ExecuteReader();
                var content = new Types.Content();

                while (reader.Read())
                {
                    content.Id = Convert.ToInt32(reader["id"]);
                    content.Title = reader["title"].ToString();
                    content.Body = reader["body"].ToString();
                    content.Active = Convert.ToBoolean(reader["active"]);
                    content.PublishDateTime = Convert.ToDateTime(reader["publish_datetime"]);
                    content.ClubArea = new ClubArea
                    {
                        Id = Convert.ToInt32(reader["club_area_id"]),
                        Name = reader["club_area_name"].ToString()
                    };
                    content.Featured = Convert.ToBoolean(reader["featured"]);
                }
                reader.Close();

                return content;
            }    
        }

        public List<Types.Content> SearchContentTitle(bool activeOnly, string searchTerm)
        {
            var contentPageSize = _settings.Database.ContentPageSize;
            var activeConstraint = activeOnly == true ? " AND c.active = @active" : "";

            var commandText = "SELECT c.id as id, ca.id as club_area_id, ca.name as club_area_name, c.title as title," +
                " c.body as body, c.publish_datetime as publish_datetime, c.active as active," +
                " c.featured as featured" +
                " FROM content c" +
                " INNER JOIN club_area ca ON c.club_area_id = ca.id" +
                " INNER JOIN club club ON ca.club_id = club.id" +
                " WHERE club.id = @clubId" +
                activeConstraint +
                " AND c.title LIKE @searchTerm" + 
                " ORDER BY c.publish_datetime DESC" +
                " LIMIT @contentPageSize";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubId", _settings.ClubId);
                command.Parameters.AddWithValue("@contentPageSize", contentPageSize);
                command.Parameters.AddWithValue("@active", 1);
                command.Parameters.AddWithValue("@searchTerm", $"%{searchTerm}%");
                
                MySqlDataReader reader = command.ExecuteReader();
                var contentList = buildContentList(reader);
                reader.Close();

                return contentList;
            }
        }

        public List<Types.Content> SearchContentBody(bool activeOnly, string searchTerm)
        {
            var contentPageSize = _settings.Database.ContentPageSize;
            var activeConstraint = activeOnly == true ? " AND c.active = @active" : "";

            var commandText = "SELECT c.id as id, ca.id as club_area_id, ca.name as club_area_name, c.title as title," +
                " c.body as body, c.publish_datetime as publish_datetime, c.active as active," +
                " c.featured as featured" +
                " FROM content c" +
                " INNER JOIN club_area ca ON c.club_area_id = ca.id" +
                " INNER JOIN club club ON ca.club_id = club.id" +
                " WHERE club.id = @clubId" +
                activeConstraint +
                " AND c.body LIKE @searchTerm" +
                " ORDER BY c.publish_datetime DESC" +
                " LIMIT @contentPageSize";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubId", _settings.ClubId);
                command.Parameters.AddWithValue("@contentPageSize", contentPageSize);
                command.Parameters.AddWithValue("@active", 1);
                command.Parameters.AddWithValue("@searchTerm", $"%{searchTerm}%");

                MySqlDataReader reader = command.ExecuteReader();
                var contentList = buildContentList(reader);
                reader.Close();

                return contentList;
            }
        }

        public List<Types.Content> SearchContentByDate(bool activeOnly, DateTime date)
        {
            var contentPageSize = _settings.Database.ContentPageSize;
            var activeConstraint = activeOnly == true ? " AND c.active = @active" : "";
            var fromDate = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            var toDate = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);

            var commandText = "SELECT c.id as id, ca.id as club_area_id, ca.name as club_area_name, c.title as title," +
                " c.body as body, c.publish_datetime as publish_datetime, c.active as active," +
                " c.featured as featured" +
                " FROM content c" +
                " INNER JOIN club_area ca ON c.club_area_id = ca.id" +
                " INNER JOIN club club ON ca.club_id = club.id" +
                " WHERE club.id = @clubId" + 
                " AND (c.publish_datetime BETWEEN @fromDate AND @toDate)" +
                activeConstraint +
                " LIMIT @contentPageSize";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubId", _settings.ClubId);
                command.Parameters.AddWithValue("@contentPageSize", contentPageSize);
                command.Parameters.AddWithValue("@active", 1);
                command.Parameters.AddWithValue("@fromDate", fromDate);
                command.Parameters.AddWithValue("@toDate", toDate);

                MySqlDataReader reader = command.ExecuteReader();
                var contentList = buildContentList(reader);
                reader.Close();

                return contentList;
            }
        }

        private static List<Types.Content> buildContentList(MySqlDataReader reader)
        {
            var contentList = new List<Types.Content>();

            while (reader.Read())
            {
                var content = new Types.Content
                {
                    Id = Convert.ToInt32(reader["id"]),
                    Title = reader["title"].ToString(),
                    Body = reader["body"].ToString(),
                    Active = Convert.ToBoolean(reader["active"]),
                    PublishDateTime = Convert.ToDateTime(reader["publish_datetime"]),
                    ClubArea = new ClubArea
                    {
                        Id = Convert.ToInt32(reader["club_area_id"]),
                        Name = reader["club_area_name"].ToString()
                    },
                    Featured = Convert.ToBoolean(reader["featured"])
                };
                contentList.Add(content);
            }
            
            return contentList;
        }
    }
}
