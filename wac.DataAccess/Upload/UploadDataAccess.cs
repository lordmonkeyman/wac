﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.DataAccess.Upload
{
    public class UploadDataAccess : IUploadDataAccess
    {
        private readonly Settings _settings;
        private readonly int _clubId;

        public UploadDataAccess(ISettingsProvider settingsProvider)
        {
            _settings = settingsProvider.Get();
            _clubId = _settings.ClubId;
        }

        public async Task AddUpload(Types.Upload upload)
        {
            var commandText = "INSERT INTO uploads" +
                " (club_id, path, file_name)" +
                " VALUES(@clubId, @path, @fileName);";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubId", _clubId);
                command.Parameters.AddWithValue("@path", upload.Path);
                command.Parameters.AddWithValue("@fileName", upload.FileName);

                await command.ExecuteNonQueryAsync();
            }
        }

        public bool DeleteUpload(int id)
        {
            var commandText = "DELETE FROM uploads WHERE id = @id;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();
                command.Parameters.AddWithValue("@id", id);
                var rowsAffected = command.ExecuteNonQuery();

                return rowsAffected == 1;
            }
        }

        public Types.Upload GetUpload(int id)
        {
            var commandText = "SELECT id, club_id, path, file_name, datetime_uploaded" + 
                " FROM uploads WHERE id = @id;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@id", id);
                MySqlDataReader reader = command.ExecuteReader();

                var upload = new Types.Upload();

                while (reader.Read())
                {
                    upload.Id = Convert.ToInt32(reader["id"]);
                    upload.ClubId = Convert.ToInt32(reader["club_id"]);
                    upload.Path = reader["path"].ToString();
                    upload.FileName = reader["file_name"].ToString();
                    upload.DatetimeUploaded = Convert.ToDateTime(reader["datetime_uploaded"]);
                }
                reader.Close();

                return upload;
            }

        }

        public List<Types.Upload> GetUploads()
        {
            var commandText = "SELECT id, club_id, path, file_name, datetime_uploaded FROM uploads WHERE club_id = @clubId;";

            using (var connection = new MySqlConnection(_settings.Database.WacConnectionString))
            using (var command = new MySqlCommand(commandText, connection))
            {
                connection.Open();

                command.Parameters.AddWithValue("@clubId", _clubId);
                MySqlDataReader reader = command.ExecuteReader();

                List<Types.Upload> uploads = new List<Types.Upload>();

                while (reader.Read())
                {
                    uploads.Add(new Types.Upload
                    {
                        Id = Convert.ToInt32(reader["id"]),
                        ClubId = Convert.ToInt32(reader["club_id"]),
                        Path = reader["path"].ToString(),
                        FileName = reader["file_name"].ToString(),
                        DatetimeUploaded = Convert.ToDateTime(reader["datetime_uploaded"])
                    });
                }
                reader.Close();

                return uploads;
            }
        }
    }
}
