﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.DataAccess.MessageBoards
{
    public class MessageBoardDataAccess : IMessageBoardsDataAccess
    {
        private readonly Settings _settings;
        private MySqlConnection _connection;
        private readonly IMessagesDataAccess _messagesDataAccess;

        public MessageBoardDataAccess(ISettingsProvider settingsProvider, IMessagesDataAccess messagesDataAccess)
        {
            _settings = settingsProvider.Get();
            _messagesDataAccess = messagesDataAccess;

            _connection = new MySqlConnection
            {
                ConnectionString = _settings.Database.WacConnectionString
            };
            _connection.Open();
        }

        public List<MessageBoard> Fetch()
        {
            var commandText = "SELECT id,"
                + " name,"
                + " club_id,"
                + " description"
                + " FROM message_boards"
                + " WHERE club_id = @clubId";

            var command = new MySqlCommand(commandText, _connection);
            command.Parameters.AddWithValue("@clubId", _settings.ClubId);

            MySqlDataReader reader = command.ExecuteReader();
            var messageBoardList = new List<MessageBoard>();

            while (reader.Read())
            {
                var messageBoard = new MessageBoard
                {
                    Id = Convert.ToInt32(reader["id"]),
                    Name = reader["name"].ToString(),
                    ClubId = Convert.ToInt32(reader["club_id"].ToString()),
                    Description = reader["description"].ToString()
                };
                messageBoardList.Add(messageBoard);
            }
            reader.Close();

            return messageBoardList;
        }

        public MessageBoard Get(int id)
        {
            var commandText = "SELECT id,"
                + " name,"
                + " club_id,"
                + " description"
                + " FROM message_boards"
                + " WHERE club_id = @clubId"
                + " AND id = @id;";

            var command = new MySqlCommand(commandText, _connection);
            command.Parameters.AddWithValue("@clubId", _settings.ClubId);
            command.Parameters.AddWithValue("@id", id);

            MySqlDataReader reader = command.ExecuteReader();
            MessageBoard messageBoard = new MessageBoard();

            while (reader.Read())
            {
                messageBoard = new MessageBoard
                {
                    Id = Convert.ToInt32(reader["id"]),
                    Name = reader["name"].ToString(),
                    ClubId = Convert.ToInt32(reader["club_id"].ToString()),
                    Description = reader["description"].ToString(),
                    Messages = _messagesDataAccess.Fetch(id)
                };
            }
            reader.Close();

            return messageBoard;
        }
    }
}
