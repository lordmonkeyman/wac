﻿using MySql.Data.MySqlClient;

namespace wac.DataAccess
{
    public interface IConnection
    {
        MySqlConnection Connect();
    }
}