-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 178.128.163.219    Database: wacdev
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audit`
--

DROP TABLE IF EXISTS `audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_datetime` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `audit_action_id` int(11) NOT NULL,
  `table_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit`
--

LOCK TABLES `audit` WRITE;
/*!40000 ALTER TABLE `audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_action`
--

DROP TABLE IF EXISTS `audit_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  `club_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_action`
--

LOCK TABLES `audit_action` WRITE;
/*!40000 ALTER TABLE `audit_action` DISABLE KEYS */;
INSERT INTO `audit_action` VALUES (1,'Create',1),(2,'Delete',1),(3,'Update',1);
/*!40000 ALTER TABLE `audit_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `club`
--

DROP TABLE IF EXISTS `club`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `club` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `club`
--

LOCK TABLES `club` WRITE;
/*!40000 ALTER TABLE `club` DISABLE KEYS */;
INSERT INTO `club` VALUES (1,'Weston Athletic Club');
/*!40000 ALTER TABLE `club` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `club_area`
--

DROP TABLE IF EXISTS `club_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `club_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `club_area_club_idx` (`club_id`),
  CONSTRAINT `club_area_club` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `club_area`
--

LOCK TABLES `club_area` WRITE;
/*!40000 ALTER TABLE `club_area` DISABLE KEYS */;
INSERT INTO `club_area` VALUES (1,1,'Main','Main area for general content about the club.'),(2,1,'Triathlon','Weston AC\'s Triathlon Content'),(3,1,'TWAC','Trail Weston AC\'s content');
/*!40000 ALTER TABLE `club_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_area_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `body` varchar(2000) CHARACTER SET utf8 DEFAULT NULL,
  `publish_datetime` datetime DEFAULT NULL,
  `active` tinyint(3) unsigned zerofill DEFAULT NULL,
  `featured` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_content_club_area_idx` (`club_area_id`),
  CONSTRAINT `fk_content_club_area` FOREIGN KEY (`club_area_id`) REFERENCES `club_area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (117,1,'Press release','[Summers represents England](uploads/WACPR20181007%20Kieron.pdf)\r\n\r\nWeston Athletic Club\'s Kieron Summers got to wear an England vest on Sunday as he represented the country at the 2018 England Age Group Masters Marathon at Chester. Having qualified for the event last year, Kieron has devoted  much of his spare time to training in preparation for the race...\r\n[read more](uploads\\WACPR20181007%20Kieron.pdf)\r\n\r\nIf you have a comment about these reports or wish to contribute to the press releases please Click\r\n[send an email enquiry to Media Team](mailto:media@westonac.co.uk?subject=Media%20Team%20-%20Enquiry) as protocol)','2018-10-10 00:00:00',001,1),(118,1,'New members','Please extend a warm welcome to new club members **Jo Radcliffe, Nicki Davis, Valerie Davis** and **Matt Davies**.','2018-10-10 00:00:00',001,1),(119,1,'One Mile Prom Race - Sept 2018','Congratulations to all the participants of the first  1 Mile Prom Run of the Series 2018-19 on Thursday 20th Sept 2018.\r\n A link to the final results is below.  If you have any queries with the results then please use the email link below and we will investigate your issue.\r\nWell Done to you all and we look forward to seeing you back at the next race in the Series\r\n\r\n**Male:**\\\r\n1st - Ollie CAMPBELL (Year 9) - North Somerset AC - 5m 29s\\\r\n2nd - Charles BROADHURST (Year 9) - North Somerset AC - 5m 39\\\r\n3rd - Louie BRUNSDON (Year 9) - North Somerset AC - 5m 44s\r\n\r\n**Female:**\\\r\n1st - Stephanie BROOKS (Year 10) - North Somerset AC - 5m 54s\\\r\n2nd - Keira DEVEREUX (Year 8) - North Somerset AC - 5m 56s\\\r\n3rd - Poppy BURTON-DICKIE (Year 10) - North Somerset AC - 6m 19s\r\n\r\nThere were 57  finishers for this 1st one mile prom in the season.\r\n\r\n[Final Results](uploads\\http://www.westonac.co.uk/results/2018/Prom_1_Mile_Sep_2018_Final.pdf) | \r\n[E-mail Prom Entry Sec with Prom Results Query](mailto:promrun@westonac.co.uk?subject=1%20Mile%20Prom%20-%20Sept%202018%20-%20Results%20Query) | \r\n[Prom Page](http://www.westonac.co.uk/PromRun/) | \r\n[Paper Entry Form](http://www.westonac.co.uk/Webadmin_docs/Prom_1_Mile/2018/WAC_Prom_Run_Mile_Entry_Form_2018_v1.pdf)','2018-10-10 00:00:00',001,1);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_priority`
--

DROP TABLE IF EXISTS `content_priority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_priority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_id` int(11) NOT NULL,
  `ordinal` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_content_priority_club_idx` (`club_id`),
  CONSTRAINT `fk_content_priority_club` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_priority`
--

LOCK TABLES `content_priority` WRITE;
/*!40000 ALTER TABLE `content_priority` DISABLE KEYS */;
INSERT INTO `content_priority` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5);
/*!40000 ALTER TABLE `content_priority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `domain_name` varchar(100) NOT NULL,
  `api_key` varchar(100) NOT NULL,
  `from` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  `club_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_club_idx` (`club_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email`
--

LOCK TABLES `email` WRITE;
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
INSERT INTO `email` VALUES (1,'Authentication Email','sandboxb0dc208c033e4cdc8c051d258c10a0d0.mailgun.org','key-9422c3dbfa5c56f98870c138ce1b2405','WAC <mailgun@sandboxb0dc208c033e4cdc8c051d258c10a0d0.mailgun.org>','Your wac login token','Supplied by application',1);
/*!40000 ALTER TABLE `email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_boards`
--

DROP TABLE IF EXISTS `message_boards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_boards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `club_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_board_club_idx` (`club_id`),
  CONSTRAINT `fk_message_board_club` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_boards`
--

LOCK TABLES `message_boards` WRITE;
/*!40000 ALTER TABLE `message_boards` DISABLE KEYS */;
INSERT INTO `message_boards` VALUES (1,'Races',1,'Do you have a race you want to tell members about? Are you looking for an entry to a race or have one you don\'t need?  Or do you need a lift to a race?'),(2,'Training',1,'Planned training sessions, arranging meet-ups for long runs, reps or fartlek.'),(3,'Social',1,'Have a non-running social event to share; quizes, charity nights, other sports.'),(4,'General',1,'Messages that don\'t fit into any other board.');
/*!40000 ALTER TABLE `message_boards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_board_id` int(11) NOT NULL,
  `parent_id` int(11) unsigned zerofill NOT NULL,
  `title` varchar(45) NOT NULL,
  `text` varchar(2000) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_datetime` (`created_datetime`),
  KEY `parent_id` (`parent_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,1,00000000000,'ddddddddddddddddddd','ddddddddddddddddd',1,'2018-04-15 19:28:02'),(2,1,00000000000,'ewrewtweryt','erytryteryter',1,'2018-04-15 19:32:46'),(3,1,00000000000,'A mor ereal message','and some text',1,'2018-04-15 20:15:49'),(4,1,00000000000,'fdsgfdsg','fgfdsgfdsgfd',1,'2018-04-15 20:16:58'),(5,2,00000000000,'fdsgfdsgfds','fdgfdsgfdsgfds',1,'2018-04-15 20:18:22'),(6,2,00000000000,'fgfdsgfdg','fdhgfdshgfshhgfsh',1,'2018-04-19 20:49:15');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_area_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,1,'Administrator'),(2,1,'Content Administrator');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploads`
--

DROP TABLE IF EXISTS `uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `datetime_uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `uploads_club_idx` (`club_id`),
  KEY `datetime_uploaded` (`datetime_uploaded`),
  CONSTRAINT `uploads_club` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploads`
--

LOCK TABLES `uploads` WRITE;
/*!40000 ALTER TABLE `uploads` DISABLE KEYS */;
INSERT INTO `uploads` VALUES (4,1,'C:\\git\\wac\\wac\\wwwroot\\uploads\\cracker.jpg','cracker.jpg','2018-10-09 12:34:43'),(5,1,'C:\\git\\wac\\wac\\wwwroot\\uploads\\Untitled.png','Untitled.png','2018-10-09 15:38:02'),(14,1,'C:\\git\\wac\\wac\\wwwroot\\uploads\\cader_idris.jpg','cader_idris.jpg','2018-10-10 08:06:19'),(15,1,'C:\\git\\wac\\wac\\wwwroot\\uploads\\22 Miles.tcx','22 Miles.tcx','2018-10-10 08:06:37'),(16,1,'C:\\git\\wac\\wac\\wwwroot\\uploads\\Whole school - Wessex Learning  Trust Transport wider community survey.pdf','Whole school - Wessex Learning  Trust Transport wider community survey.pdf','2018-10-10 08:07:59'),(17,1,'C:\\git\\wac\\wac\\wwwroot\\uploads\\WACPR20180311.2.pdf','WACPR20180311.2.pdf','2018-10-10 08:08:28'),(18,1,'C:\\git\\wac\\wac\\wwwroot\\uploads\\WACPR20180311.1.pdf','WACPR20180311.1.pdf','2018-10-10 08:09:10'),(19,1,'C:\\git\\wac\\wac\\wwwroot\\uploads\\WACPR20181007 Kieron.pdf','WACPR20181007 Kieron.pdf','2018-10-10 08:09:13'),(20,1,'C:\\git\\wac\\wac\\wwwroot\\uploads\\WACPR20181007 Kieron.pdf','WACPR20181007 Kieron.pdf','2018-10-10 11:59:19');
/*!40000 ALTER TABLE `uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `second_name` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `authentication_token` varchar(36) DEFAULT NULL,
  `authentication_expiry` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_type_idx` (`user_type_id`),
  CONSTRAINT `fk_user_type` FOREIGN KEY (`user_type_id`) REFERENCES `user_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,1,'Matthew','Wheeler','matthewwheeler@yahoo.co.uk','5cbba530-6df9-434d-ae1d-ef639a5054c4','2018-10-16 08:30:39'),(2,1,'Mark','Hill','markaahill@gmail.com','8696d23a-3616-4430-a8e5-62517781a99a','2018-08-29 14:27:33'),(3,1,'Gareth','Weaver','geeweaver@gmail.com','ca0f2e16-01ce-43e6-90a8-7be292ea5e4f','2018-05-18 13:09:24'),(4,1,'Sara','Weaver','squelchie@gmail.com',NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_roles_user_idx` (`user_id`),
  KEY `fk_user_roles_role_idx` (`role_id`),
  CONSTRAINT `fk_user_roles_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_roles_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,1,1),(2,1,2);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_type`
--

DROP TABLE IF EXISTS `user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `club_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `description_UNIQUE` (`description`),
  KEY `fk_club_idx` (`club_id`),
  CONSTRAINT `fk_club` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_type`
--

LOCK TABLES `user_type` WRITE;
/*!40000 ALTER TABLE `user_type` DISABLE KEYS */;
INSERT INTO `user_type` VALUES (1,'admin',1),(2,'user',1);
/*!40000 ALTER TABLE `user_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-18 15:41:28
