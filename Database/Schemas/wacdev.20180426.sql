-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema wacdev
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema wacdev
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `wacdev` DEFAULT CHARACTER SET latin1 ;
USE `wacdev` ;

-- -----------------------------------------------------
-- Table `wacdev`.`club`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wacdev`.`club` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wacdev`.`content_priority`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wacdev`.`content_priority` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `club_id` INT(11) NOT NULL,
  `ordinal` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_content_priority_club_idx` (`club_id` ASC),
  CONSTRAINT `fk_content_priority_club`
    FOREIGN KEY (`club_id`)
    REFERENCES `wacdev`.`club` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wacdev`.`content`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wacdev`.`content` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `club_id` INT(11) NOT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `body` VARCHAR(1000) NULL DEFAULT NULL,
  `publish_datetime` DATETIME NULL DEFAULT NULL,
  `active` TINYINT(3) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `priority_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_content_club_idx` (`club_id` ASC),
  INDEX `fk_content_content_priority_idx` (`priority_id` ASC),
  CONSTRAINT `fk_content_club`
    FOREIGN KEY (`club_id`)
    REFERENCES `wacdev`.`club` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_content_content_priority`
    FOREIGN KEY (`priority_id`)
    REFERENCES `wacdev`.`content_priority` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wacdev`.`email`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wacdev`.`email` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NOT NULL,
  `domain_name` VARCHAR(100) NOT NULL,
  `api_key` VARCHAR(100) NOT NULL,
  `from` VARCHAR(255) NOT NULL,
  `subject` VARCHAR(255) NOT NULL,
  `text` VARCHAR(255) NOT NULL,
  `club_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_club_idx` (`club_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wacdev`.`message_boards`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wacdev`.`message_boards` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `club_id` INT(11) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_message_board_club_idx` (`club_id` ASC),
  CONSTRAINT `fk_message_board_club`
    FOREIGN KEY (`club_id`)
    REFERENCES `wacdev`.`club` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wacdev`.`messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wacdev`.`messages` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `message_board_id` INT(11) NOT NULL,
  `parent_id` INT(11) UNSIGNED ZEROFILL NOT NULL,
  `title` VARCHAR(45) NOT NULL,
  `text` VARCHAR(2000) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `created_datetime` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `created_datetime` (`created_datetime` ASC),
  INDEX `parent_id` (`parent_id` ASC),
  INDEX `user_id` (`user_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wacdev`.`user_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wacdev`.`user_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(100) NULL DEFAULT NULL,
  `club_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `description_UNIQUE` (`description` ASC),
  INDEX `fk_club_idx` (`club_id` ASC),
  CONSTRAINT `fk_club`
    FOREIGN KEY (`club_id`)
    REFERENCES `wacdev`.`club` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `wacdev`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wacdev`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` INT(11) NOT NULL,
  `first_name` VARCHAR(45) NULL DEFAULT NULL,
  `second_name` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `authentication_token` VARCHAR(36) NULL DEFAULT NULL,
  `authentication_expiry` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_type_idx` (`user_type_id` ASC),
  CONSTRAINT `fk_user_type`
    FOREIGN KEY (`user_type_id`)
    REFERENCES `wacdev`.`user_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
