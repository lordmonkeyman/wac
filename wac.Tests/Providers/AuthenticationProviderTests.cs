using System;
using Xunit;
using wac.Providers;
using Moq;
using wac.Interfaces.DataAccess.Access;
using wac.Types;
using wac.Interfaces.Providers;
using Microsoft.AspNetCore.Http;

namespace wac.Tests
{
    public class AuthenticationProviderTests
    {
        private Mock<IAuthenticationDataAccess> _authenticationDataAccess;
        private Mock<ISettingsProvider> _settingsProvider;
        private Mock<IHttpContextAccessor> _httpContextAccessor;
        private Mock<ILogProvider> _logProvider;

        public AuthenticationProviderTests()
        {
            _authenticationDataAccess = new Mock<IAuthenticationDataAccess>();
            _settingsProvider = new Mock<ISettingsProvider>();
            _httpContextAccessor = new Mock<IHttpContextAccessor>();
            _logProvider = new Mock<ILogProvider>();
        }

        [Fact]
        public void RequestToken_WhenPassedAValidEmail_ReturnsUser()
        {
            var email = "name@example.com";
            var user = new User { Id=1 };
            var settings = new Settings { Authorisation = new Authorisation { ExpiryInMinutes = 15 } };
            _authenticationDataAccess.Setup(x => x.RequestToken(email)).Returns(user);
            _settingsProvider.Setup(x => x.Get()).Returns(settings);

            var authenticationProvider = new AuthenticationProvider(_settingsProvider.Object, _authenticationDataAccess.Object, _httpContextAccessor.Object, _logProvider.Object);

            var result = authenticationProvider.RequestToken(email);

            Assert.Equal(result.Id.ToString(), user.Id.ToString());
        }
    }
}
