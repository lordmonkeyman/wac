﻿using Xunit;
using wac.Providers;
using System;

namespace wac.Tests.Providers
{
    public class DateTimeProviderTests
    {
        [Fact]
        public void ToClientSideDate_WhenPassedDateTime_ReturnsDateAsStringInTheCorrectFormat()
        {
            var datetimeProvider = new DateTimeProvider();
            var dateTime = new DateTime(1999, 12, 31, 23, 1, 59);
            var expected = "19991231230159"; // yyyyMMddHHmmss

            var result = datetimeProvider.ToClientSideDate(dateTime);

            Assert.Equal(expected, result);
        }

        [Fact]
        public void CalculateWeekEnding_WhenPassedAMondayDate_ReturnsDateTimeNextSunday()
        {
            var weekCommencing = new DateTime(2018, 10, 22);
            var datetimeProvider = new DateTimeProvider();
            var expected = new DateTime(2018, 10, 28, 23, 59, 59);

            var result = datetimeProvider.CalculateWeekEnding(weekCommencing);

            Assert.Equal(expected, result);
        }

        [Fact]
        public void CalculateWeekEnding_WhenPassedNotMondayDate_ReturnsDateTimeNextSunday()
        {
            var weekCommencing = new DateTime(2018, 10, 26);
            var datetimeProvider = new DateTimeProvider();
            var expected = new DateTime(2018, 10, 28, 23, 59, 59);

            var result = datetimeProvider.CalculateWeekEnding(weekCommencing);

            Assert.Equal(expected, result);
        }

        [Fact]
        public void CalculateWeekEnding_WhenPassedTimePortion_IgnoresIt()
        {
            var weekCommencing = new DateTime(2018, 10, 26, 11, 45, 33);
            var datetimeProvider = new DateTimeProvider();
            var expected = new DateTime(2018, 10, 28, 23, 59, 59);

            var result = datetimeProvider.CalculateWeekEnding(weekCommencing);

            Assert.Equal(expected, result);
        }

        [Fact]
        public void CalculateWeekCommencing_WhenPassedDate_ReturnsDateWeekCommenced()
        {
            var datetimeProvider = new DateTimeProvider();
            var today = new DateTime(2018, 10, 31);
            var expected = new DateTime(2018, 10, 29);

            var result = datetimeProvider.CalculateWeekCommencing(today, DayOfWeek.Monday);

            Assert.Equal(expected, result);
        }

        [Fact]
        public void GetUtcNowDateOnly_ReturnsCurrentDateWithoutTime()
        {
            var datetimeProvider = new DateTimeProvider();
            var dateTime = new DateTime(1909, 12, 13, 12, 34, 56);
            var expected = new DateTime(1909, 12, 13, 0, 0, 0);

            var result = datetimeProvider.GetDateOnly(dateTime);

            Assert.Equal(expected, result);
        }
    }
}
