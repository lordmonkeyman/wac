﻿using Xunit;
using wac.Providers;
using Moq;
using wac.Interfaces.Providers;

namespace wac.Tests.Providers
{
    public class MarkdownProviderTests
    {
        [Fact]
        public void ConvertMarkdownToHtml_WhenPassedMarkdown_ReturnsHtml()
        {
            var markDown = "This is a text with some *emphasis*";
            var markDownProvider = new MarkdownProvider();

            var result = markDownProvider.ConvertMarkdownToHtml(markDown);

            Assert.Equal("<p>This is a text with some <em>emphasis</em></p>\n", result);
        }

        [Fact]
        public void ConvertMarkdownToHtml_WhenPassedImbededHtml_DoesNotConvertIt()
        {
            var markDown = "This is a text with some *emphasis*<script>alert('BANG');</script>";
            var markDownProvider = new MarkdownProvider();

            var result = markDownProvider.ConvertMarkdownToHtml(markDown);

            Assert.Equal("<p>This is a text with some <em>emphasis</em>&lt;script&gt;alert('BANG');&lt;/script&gt;</p>\n", result);
        }

        [Fact]
        public void ConvertMarkdownToHtml_WhenPassedExtraEmphasisMarks_ConvertsAndReturnsHtml()
        {
            var markDown = "The following text ~~is deleted~~";
            var markDownProvider = new MarkdownProvider();

            var result = markDownProvider.ConvertMarkdownToHtml(markDown);

            Assert.Equal("<p>The following text <del>is deleted</del></p>\n", result);
        }

        [Fact]
        public void ConvertMarkdownToHtml_WhenPassedGenericAttributesMarks_ConvertsAndReturnsHtml()
        {
            var markDown = "# This is a heading # {#heading-link2}";
            var markDownProvider = new MarkdownProvider();

            var result = markDownProvider.ConvertMarkdownToHtml(markDown);

            Assert.Equal("<h1 id=\"heading-link2\">This is a heading</h1>\n", result);
        }

        [Fact]
        public void ConvertMarkdownToHtml_WhenPassedHttpLink_ConvertsToAnchorTag()
        {
            var markDown = "This is a http://www.google.com URL and https://www.google.com";
            var markDownProvider = new MarkdownProvider();

            var result = markDownProvider.ConvertMarkdownToHtml(markDown);

            Assert.Equal("<p>This is a <a href=\"http://www.google.com\">http://www.google.com</a> URL and <a href=\"https://www.google.com\">https://www.google.com</a></p>\n", result);
        }

        [Fact]
        public void ConvertMarkdownToHtml_WhenPassedMailToLink_ConvertsToAnchorTag()
        {
            var markDown = "And a mailto:email@toto.com";
            var markDownProvider = new MarkdownProvider();

            var result = markDownProvider.ConvertMarkdownToHtml(markDown);

            Assert.Equal("<p>And a <a href=\"mailto:email@toto.com\">email@toto.com</a></p>\n", result);
        }
    }
}
