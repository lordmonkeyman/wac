﻿using System;
using Xunit;
using wac.Providers;
using Moq;
using wac.Types;
using wac.Interfaces.Providers;
using wac.Handlers.Messages;
using System.Collections.Generic;

namespace wac.Tests.Handlers
{
    public class MessagesHandlerTests
    {
        private Mock<IMessageBoardsProvider> _messageBoardProvider;
        private Mock<IMessagesProvider> _messagesProvider;

        public MessagesHandlerTests()
        {
            _messageBoardProvider = new Mock<IMessageBoardsProvider>();
            _messagesProvider = new Mock<IMessagesProvider>();
        }

        [Fact]
        public void GetMessageBoard_WhenPassedAValidId_ReturnsMessageBoardAndMessages()
        {
            var id = 1;
            var messages = new List<Message> { new Message { } };
            var messageBoard = new MessageBoard { Id = 1, Messages = messages};

            _messageBoardProvider.Setup(x => x.GetMessageBoard(id)).Returns(messageBoard);

            var messagesHandler = new MessagesHandler(_messagesProvider.Object, _messageBoardProvider.Object);

            var result = messagesHandler.GetMessageBoard(id);

            Assert.Equal(id, result.Id);
            Assert.Single(result.Messages);
        }

        [Fact]
        public void GetMessageBoard_WhenPassedAInValidId_ReturnsNull()
        {
            var id = 99;
            var messageBoard = new MessageBoard { Id = 0 };

            _messageBoardProvider.Setup(x => x.GetMessageBoard(id)).Returns(messageBoard);

            var messagesHandler = new MessagesHandler(_messagesProvider.Object, _messageBoardProvider.Object);

            var result = messagesHandler.GetMessageBoard(id);

            Assert.Null(result);
        }
    }
}
