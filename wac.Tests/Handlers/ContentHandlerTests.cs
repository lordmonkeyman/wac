﻿using Moq;
using wac.Types.Constants;
using wac.Handlers.Content;
using wac.Interfaces.Providers;
using Xunit;
using wac.Types.Exceptions;
using System;
using System.Collections.Generic;

namespace wac.Tests.Handlers
{
    public class ContentHandlerTests
    {
        private Mock<IContentProvider> _contentProvider;
        private Mock<IClubProvider> _clubProvider;
        private Mock<ISettingsProvider> _settingsProvider;
        private Mock<IAuthenticationProvider> _authenticationProvider;
        private Mock<IMarkdownProvider> _markdownProvider;

        public ContentHandlerTests()
        {
            _contentProvider = new Mock<IContentProvider>();
            _clubProvider = new Mock<IClubProvider>();
            _settingsProvider = new Mock<ISettingsProvider>();
            _authenticationProvider = new Mock<IAuthenticationProvider>();
            _markdownProvider = new Mock<IMarkdownProvider>();
        }

        [Fact]
        public void SearchContent_WhenPassedValidDate_CallsSearchContentByDate()
        {
            var activeOnly = true;
            var searchTerm = "10/10/2018";
            var searchTermAsDate = Convert.ToDateTime(searchTerm);

            _contentProvider.Setup(x => x.SearchContentByDate(activeOnly, searchTermAsDate)).Returns(new List<Types.Content>()).Verifiable();
            
            var contentHandler = new ContentHandler(
                _contentProvider.Object,
                _clubProvider.Object,
                _settingsProvider.Object,
                _authenticationProvider.Object,
                _markdownProvider.Object);

            var result = contentHandler.SearchContent(activeOnly, searchTerm);

            _contentProvider.Verify(x => x.SearchContentByDate(activeOnly, searchTermAsDate), Times.Once);
        }

        [Fact]
        public void SearchContent_WhenPassedNonDate_CallsSearchContentTitle()
        {
            var activeOnly = true;
            var searchTerm = "Trousers";
            var contents = new List<Types.Content>();
            contents.Add(new Types.Content());

            _contentProvider.Setup(x => x.SearchContentTitle(activeOnly, searchTerm)).Returns(contents).Verifiable();

            var contentHandler = new ContentHandler(
                _contentProvider.Object,
                _clubProvider.Object,
                _settingsProvider.Object,
                _authenticationProvider.Object,
                _markdownProvider.Object);

            var result = contentHandler.SearchContent(activeOnly, searchTerm);

            _contentProvider.Verify(x => x.SearchContentTitle(activeOnly, searchTerm), Times.Once);
        }

        [Fact]
        public void SearchContent_WhenPassedNonDateAndTitleSearchReturnsNothing_CallsSearchContentBody()
        {
            var activeOnly = true;
            var searchTerm = "Trousers";
            var titleContents = new List<Types.Content>();
            var bodyContents = new List<Types.Content>();
            bodyContents.Add(new Types.Content());

            _contentProvider.Setup(x => x.SearchContentTitle(activeOnly, searchTerm)).Returns(titleContents);
            _contentProvider.Setup(x => x.SearchContentBody(activeOnly, searchTerm)).Returns(bodyContents).Verifiable();

            var contentHandler = new ContentHandler(
                _contentProvider.Object,
                _clubProvider.Object,
                _settingsProvider.Object,
                _authenticationProvider.Object,
                _markdownProvider.Object);

            var result = contentHandler.SearchContent(activeOnly, searchTerm);

            _contentProvider.Verify(x => x.SearchContentBody(activeOnly, searchTerm), Times.Once);
        }

        [Fact]
        public void SearchContent_WhenFindsContent_CallsMarkdownProviderForEachContent()
        {
            var activeOnly = true;
            var searchTerm = "Trousers";
            var contents = new List<Types.Content>();
            contents.Add(new Types.Content());
            contents.Add(new Types.Content());

            _contentProvider.Setup(x => x.SearchContentTitle(activeOnly, searchTerm)).Returns(contents);
            _markdownProvider.Setup(x => x.ConvertMarkdownToHtml(It.IsAny<string>())).Returns(It.IsAny<string>()).Verifiable();

            var contentHandler = new ContentHandler(
                _contentProvider.Object,
                _clubProvider.Object,
                _settingsProvider.Object,
                _authenticationProvider.Object,
                _markdownProvider.Object);

            var result = contentHandler.SearchContent(activeOnly, searchTerm);

            _markdownProvider.Verify(x => x.ConvertMarkdownToHtml(It.IsAny<string>()), Times.Exactly(2));
        }

        [Fact]
        public void CreateContent_WhenInCorrectRole_CallsContentProvider()
        {
            var content = new Types.Content { Id = 0 };
            var createdContent = new Types.Content { Id = 1 };

            _contentProvider.Setup(x => x.CreateContent(content)).Returns(createdContent);
            _authenticationProvider.Setup(x => x.HasRole(Roles.CONTENT_ADMINISTRATOR)).Returns(true);

            var contentHandler = new ContentHandler(
                _contentProvider.Object,
                _clubProvider.Object,
                _settingsProvider.Object,
                _authenticationProvider.Object,
                _markdownProvider.Object);

            var result = contentHandler.CreateContent(content);

            Assert.Equal(1, result.Id);
        }

        [Fact]
        public void CreateContent_WhenNotInCorrectRole_ThrowsNotAuthorisedException()
        {
            var content = new Types.Content { Id = 0 };
            var createdContent = new Types.Content { Id = 1 };

            _contentProvider.Setup(x => x.CreateContent(content)).Returns(createdContent);
            _authenticationProvider.Setup(x => x.HasRole(Roles.CONTENT_ADMINISTRATOR)).Returns(false);

            var contentHandler = new ContentHandler(
                _contentProvider.Object,
                _clubProvider.Object,
                _settingsProvider.Object,
                _authenticationProvider.Object,
                _markdownProvider.Object);

            Assert.Throws<NotAuthorisedException>(() => contentHandler.CreateContent(content));
        }

        [Fact]
        public void DeleteContent_WhenInCorrectRole_CallsContentProvider()
        {
            var content = new Types.Content { Id = 0 };
            
            _contentProvider.Setup(x => x.DeleteContent(content.Id)).Returns(true);
            _authenticationProvider.Setup(x => x.HasRole(Roles.CONTENT_ADMINISTRATOR)).Returns(true);

            var contentHandler = new ContentHandler(
                _contentProvider.Object,
                _clubProvider.Object,
                _settingsProvider.Object,
                _authenticationProvider.Object,
                _markdownProvider.Object);

            var result = contentHandler.DeleteContent(content.Id);

            Assert.True(result);
        }

        [Fact]
        public void DeleteContent_WhenNotInCorrectRole_ThrowsNotAuthorisedException()
        {
            _authenticationProvider.Setup(x => x.HasRole(Roles.CONTENT_ADMINISTRATOR)).Returns(false);

            var contentHandler = new ContentHandler(
                _contentProvider.Object,
                _clubProvider.Object,
                _settingsProvider.Object,
                _authenticationProvider.Object,
                _markdownProvider.Object);

            Assert.Throws<NotAuthorisedException>(() => contentHandler.DeleteContent(99));
        }

        [Fact]
        public void UpdateContent_WhenNotInCorrectRole_ThrowsNotAuthorisedException()
        {
            var content = new Types.Content { Id = 0 };

            _authenticationProvider.Setup(x => x.HasRole(Roles.CONTENT_ADMINISTRATOR)).Returns(false);

            var contentHandler = new ContentHandler(
                _contentProvider.Object,
                _clubProvider.Object,
                _settingsProvider.Object,
                _authenticationProvider.Object,
                _markdownProvider.Object);

            Assert.Throws<NotAuthorisedException>(() => contentHandler.UpdateContent(content));
        }

        [Fact]
        public void UpdateContent_WhenInCorrectRole_CallsContentProvider()
        {
            var content = new Types.Content { Id = 99 };
            
            _contentProvider.Setup(x => x.UpdateContent(content)).Returns(true);
            _authenticationProvider.Setup(x => x.HasRole(Roles.CONTENT_ADMINISTRATOR)).Returns(true);

            var contentHandler = new ContentHandler(
                _contentProvider.Object,
                _clubProvider.Object,
                _settingsProvider.Object,
                _authenticationProvider.Object,
                _markdownProvider.Object);

            var result = contentHandler.UpdateContent(content);

            Assert.True(result);
        }
    }
}
