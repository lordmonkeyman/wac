﻿using Moq;
using wac.Types;
using wac.Types.Constants;
using wac.Handlers.Access;
using wac.Interfaces.Providers;
using Xunit;
using wac.Types.Exceptions;
using Microsoft.AspNetCore.Hosting;
using System;

namespace wac.Tests.Handlers
{
    public class AuthenticationHandlerTests
    {
        private Mock<ISettingsProvider> _settingsProvider;
        private Mock<IAuthenticationProvider> _authenticationProvider;
        private Mock<ILogProvider> _logProvider;
        private Mock<IEmailProvider> _emailProvider;
        private Mock<IHostingEnvironment> _hostingEnvironment;
        private Mock<IDateTimeProvider> _dateTimeProvider;

        public AuthenticationHandlerTests()
        {
            _settingsProvider = new Mock<ISettingsProvider>();
            _authenticationProvider = new Mock<IAuthenticationProvider>();
            _logProvider = new Mock<ILogProvider>();
            _emailProvider = new Mock<IEmailProvider>();
            _hostingEnvironment = new Mock<IHostingEnvironment>();
            _dateTimeProvider = new Mock<IDateTimeProvider>();
        }

        [Fact]
        public void RequestAuthentication_PasswordDummyFieldIsNotEmpty_DoesNotSendEmail()
        {
            var loginRequest = new LoginRequest { Password = "pwd" };
            var email = new Email();
            _emailProvider.Setup(x => x.SendEmailAsync(email)).Verifiable();

            var authenticationHandler = new AuthenticationHandler(
                _settingsProvider.Object, 
                _logProvider.Object, 
                _authenticationProvider.Object, 
                _emailProvider.Object, 
                _hostingEnvironment.Object,
                _dateTimeProvider.Object);

            authenticationHandler.RequestAuthentication(loginRequest);

            _emailProvider.Verify(x => x.SendEmailAsync(email), Times.Never);
        }

        [Fact]
        public void RequestAuthentication_EmailDummyFieldIsNotEmpty_DoesNotSendEmail()
        {
            var loginRequest = new LoginRequest { Email = "email" };
            var email = new Email();
            _emailProvider.Setup(x => x.SendEmailAsync(email)).Verifiable();

            var authenticationHandler = new AuthenticationHandler(
                _settingsProvider.Object,
                _logProvider.Object,
                _authenticationProvider.Object,
                _emailProvider.Object,
                _hostingEnvironment.Object,
                _dateTimeProvider.Object);

            authenticationHandler.RequestAuthentication(loginRequest);

            _emailProvider.Verify(x => x.SendEmailAsync(email), Times.Never);
        }

        [Fact]
        public void RequestAuthentication_WhenTimestampIsInIncorrectFormat_DoesNotSendEmail()
        {
            var loginRequest = new LoginRequest { TimeStamp = "rubbish" };

            var email = new Email();
            _emailProvider.Setup(x => x.SendEmailAsync(email)).Verifiable();

            var authenticationHandler = new AuthenticationHandler(
                _settingsProvider.Object,
                _logProvider.Object,
                _authenticationProvider.Object,
                _emailProvider.Object,
                _hostingEnvironment.Object,
                _dateTimeProvider.Object);

            authenticationHandler.RequestAuthentication(loginRequest);

            _emailProvider.Verify(x => x.SendEmailAsync(email), Times.Never);
        }

        [Fact]
        public void RequestAuthentication_WhenTimestampIsGreaterThan20MinsInThePast_DoesNotSendEmail()
        {
            var loginRequest = new LoginRequest { TimeStamp = "0216100918A072119" }; // 19 is the minute portion
            DateTime.TryParseExact("6374120918P442116", Constants.TIMESTAMP_FORMAT, null, System.Globalization.DateTimeStyles.None, out DateTime futureDate);

            var user = new User { AuthenticationToken = Guid.NewGuid(), Email = "email" };
            _authenticationProvider.Setup(x => x.RequestToken(loginRequest.Token)).Returns(user);

            var settings = new Settings { HostName = "hostname" };
            _settingsProvider.Setup(x => x.Get()).Returns(settings);

            var email = new Email();
            _emailProvider.Setup(x => x.GetEmailProperties(Constants.AUTHENTICATION_EMAIL_ID)).Returns(email);
            _emailProvider.Setup(x => x.SendEmailAsync(email)).Verifiable();
            _dateTimeProvider.Setup(y => y.GetUtcNow()).Returns(futureDate);

            var authenticationHandler = new AuthenticationHandler(
                _settingsProvider.Object,
                _logProvider.Object,
                _authenticationProvider.Object,
                _emailProvider.Object,
                _hostingEnvironment.Object,
                _dateTimeProvider.Object);

            authenticationHandler.RequestAuthentication(loginRequest);

            _emailProvider.Verify(x => x.SendEmailAsync(email), Times.Never);
        }

        [Fact]
        public void RequestAuthentication_WhenRequestIsValid_SendsEmail()
        {
            var loginRequest = new LoginRequest { Token="email", Email="", Password="", TimeStamp = "0216100918A072119" }; // 19 is the minute portion
            DateTime.TryParseExact("0216100918A072119", Constants.TIMESTAMP_FORMAT, null, System.Globalization.DateTimeStyles.None, out DateTime utcNow);
            _dateTimeProvider.Setup(y => y.GetUtcNow()).Returns(utcNow);

            var user = new User { AuthenticationToken = Guid.NewGuid(), Email = "email" };
            _authenticationProvider.Setup(x => x.RequestToken(loginRequest.Token)).Returns(user);

            var settings = new Settings { HostName = "hostname" };
            _settingsProvider.Setup(x => x.Get()).Returns(settings);

            var email = new Email();
            _emailProvider.Setup(x => x.GetEmailProperties(Constants.AUTHENTICATION_EMAIL_ID)).Returns(email);
            _emailProvider.Setup(x => x.SendEmailAsync(email)).Verifiable();

            var authenticationHandler = new AuthenticationHandler(
                _settingsProvider.Object,
                _logProvider.Object,
                _authenticationProvider.Object,
                _emailProvider.Object,
                _hostingEnvironment.Object,
                _dateTimeProvider.Object);

            authenticationHandler.RequestAuthentication(loginRequest);

            _emailProvider.Verify(x => x.SendEmailAsync(email), Times.Once);
        }
    }
}
