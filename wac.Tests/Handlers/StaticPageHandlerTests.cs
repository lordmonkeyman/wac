﻿using Microsoft.Extensions.Caching.Memory;
using Moq;
using wac.Handlers.StaticPage;
using wac.Interfaces.Providers;
using Xunit;
using wac.Types;

namespace wac.Tests.Handlers
{
    public class StaticPageHandlerTests
    {
        private Mock<IStaticPageProvider> _staticPageProvider;
        private Mock<ISettingsProvider> _settingsProvider;
        private Mock<IDateTimeProvider> _dateTimeProvider;

        public StaticPageHandlerTests()
        {
            _staticPageProvider = new Mock<IStaticPageProvider>();
            _settingsProvider = new Mock<ISettingsProvider>();
            _dateTimeProvider = new Mock<IDateTimeProvider>();
        }

        [Fact]
        private void GetContent_WhenPageIsCachedButCachingIsTurnedOff_DoesNotSetCacheAndReturnsContentFromProvider()
        {
            var folder = "FOLDER";
            var page = "PAGE";
            var content = "CONTENT";
            var cacheKey = $"{folder}-{page}";
            var cachedContent = "CACHEDCONTENT";

            var cachEntry = Mock.Of<ICacheEntry>();

            var settings = new Settings { Caching = new Caching { UseCaching = false, Expiration = 99 } };
            _settingsProvider.Setup(x => x.Get()).Returns(settings);

            _staticPageProvider.Setup(x => x.GetContent(folder, page)).Returns(content).Verifiable();

            var memoryCache = MockMemoryCacheService.GetMemoryCacheTrue(cachedContent);
            
            var staticPageHandler = new StaticPageHandler(_staticPageProvider.Object, memoryCache, _settingsProvider.Object, _dateTimeProvider.Object);

            var result = staticPageHandler.GetContent(folder, page);

            Assert.Equal(content, result);
            _staticPageProvider.Verify(x => x.GetContent(folder, page), Times.Once);
        }

        [Fact]
        private void GetContent_WhenPageIsNotCached_SetsCacheAndReturnsContentFromProvider()
        {
            var folder = "FOLDER";
            var page = "PAGE";
            var content = "CONTENT";
            var cacheKey = $"{folder}-{page}";
            var cachedContent = "CACHEDCONTENT";

            var cachEntry = Mock.Of<ICacheEntry>();

            var settings = new Settings { Caching = new Caching { UseCaching = true, Expiration = 99 } };
            _settingsProvider.Setup(x => x.Get()).Returns(settings);

            _staticPageProvider.Setup(x => x.GetContent(folder, page)).Returns(content).Verifiable();
            
            var memoryCache = MockMemoryCacheService.GetMemoryCacheFalse(cachedContent);
            var mockMemoryCache = Mock.Get(memoryCache);
            mockMemoryCache
                    .Setup(m => m.CreateEntry(It.IsAny<object>()))
                    .Returns(cachEntry);

            var staticPageHandler = new StaticPageHandler(_staticPageProvider.Object, memoryCache, _settingsProvider.Object, _dateTimeProvider.Object);

            var result = staticPageHandler.GetContent(folder, page);

            Assert.Equal(content, result);
            _staticPageProvider.Verify(x => x.GetContent(folder, page), Times.Once);
        }

        [Fact]
        private void GetContent_WhenPageIsCached_ReturnsCachedContent()
        {
            var folder = "FOLDER";
            var page = "PAGE";
            var content = "CONTENT";
            var cacheKey = $"{folder}-{page}";
            var cachedContent = "CACHEDCONTENT";

            var settings = new Settings { Caching = new Caching { UseCaching = true, Expiration = 99 } };
            _settingsProvider.Setup(x => x.Get()).Returns(settings);

            _staticPageProvider.Setup(x => x.GetContent(folder, page)).Returns(content).Verifiable();
            
            var memoryCache = MockMemoryCacheService.GetMemoryCacheTrue(cachedContent);

            var staticPageHandler = new StaticPageHandler(_staticPageProvider.Object, memoryCache, _settingsProvider.Object, _dateTimeProvider.Object);

            var result = staticPageHandler.GetContent(folder, page);

            Assert.Equal(cachedContent, result);
            _staticPageProvider.Verify(x => x.GetContent(folder, page), Times.Never);
        }
    }

    public static class MockMemoryCacheService
    {
        public static TItem Set<TItem>(this IMemoryCache cache, object key, TItem value)
        {
            var entry = cache.CreateEntry(key);
            entry.Value = value;
            entry.Dispose();

            return value;
        }

        public static IMemoryCache GetMemoryCacheTrue(object expectedValue)
        {
            var mockMemoryCache = new Mock<IMemoryCache>();
            mockMemoryCache
                .Setup(x => x.TryGetValue(It.IsAny<object>(), out expectedValue))
                .Returns(true);
            return mockMemoryCache.Object;
        }

        public static IMemoryCache GetMemoryCacheFalse(object expectedValue)
        {
            var mockMemoryCache = new Mock<IMemoryCache>();
            mockMemoryCache
                .Setup(x => x.TryGetValue(It.IsAny<object>(), out expectedValue))
                .Returns(false);
            return mockMemoryCache.Object;
        }
    }
}
