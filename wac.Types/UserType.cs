﻿
namespace wac.Types
{
    public class UserType
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public Club Club { get; set; }
    }
}
