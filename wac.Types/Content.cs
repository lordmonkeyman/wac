﻿using System;
using System.Collections.Generic;

namespace wac.Types
{
    public class Content
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime PublishDateTime { get; set; }
        public string PublishDateTimeText => PublishDateTime.ToString("dddd, d MMMM yyyy");
        public bool Active { get; set; }
        public bool Featured { get; set; }
        public ClubArea ClubArea { get; set; }
        public bool IsNew { get; set; }
        public List<ClubArea> ClubAreas { get; set; }
    }
}
