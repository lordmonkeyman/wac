﻿using System;

namespace wac.Types
{
    public class LoginRequest
    {
        public string Token { get; set; }
        public string TimeStamp { get; set; }

        // dummy fields to trap spambots - token holds the real email address
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
