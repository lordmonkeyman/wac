﻿namespace wac.Types
{
    public class ClubArea
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ClubId { get; set; }
    }
}
