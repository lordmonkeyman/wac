﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wac.Types
{
    public class FlashMessage
    {
        public string Message { get; set; }
        public FlashMessageState State { get; set; }
        public string CssClass { get; set; }
    }

    public enum FlashMessageState
    {
        Unknown = 0,
        Successful = 1,
        Failed = 2
    }
}
