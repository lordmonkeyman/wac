﻿using System;

namespace wac.Types
{
    public class Event
    {
        public int Id { get; set; }
        public ClubArea ClubArea { get; set; }
        public EventType EventType { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Location { get; set; }
        public string  Description { get; set; }
        public int DayOfWeek()
        {
            if (EventDate.DayOfWeek == System.DayOfWeek.Sunday) return 7;
            return Convert.ToInt32(EventDate.DayOfWeek);
        }
    }
}
