﻿namespace wac.Types
{
    public class EventType
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
