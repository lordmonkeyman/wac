﻿using System;
using wac.Types;

namespace wac.Types.Exceptions
{
    public class NotAuthorisedException: Exception
    {
        public NotAuthorisedException(string requiredRole, string action) : base($"NotAuthorisedException: Attempt to call {action} but does not have {requiredRole} role.")
        {
            
        }
    }
}
