﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wac.Types
{
    public class Email
    {
        public int Id { get; set; }
        public string DomainName { get; set; }
        public string ApiKey { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
    }
}
