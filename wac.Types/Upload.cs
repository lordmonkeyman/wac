﻿using System;

namespace wac.Types
{
    public class Upload
    {
        public int Id { get; set; }
        public int ClubId { get; set; }
        public string Path { get; set; }
        public string Link { get; set; }
        public string FileName { get; set; }
        public DateTime DatetimeUploaded { get; set; }
    }
}
