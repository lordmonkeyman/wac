﻿
using System;
using System.Collections.Generic;

namespace wac.Types
{
    public class User
    {
        public int Id { get; set; }
        public UserType UserType { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public Guid AuthenticationToken { get; set; }
        public DateTime AuthenticationExpiry { get; set; }
        public List<Role> Roles { get; set; }
    }
}
