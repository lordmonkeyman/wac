﻿using System;

namespace wac.Types
{
    public class SocialEvent
    {
        public int Id { get; set; }
        public ClubArea ClubArea { get; set; }
        public string Title { get; set; }
        public DateTime EventDate { get; set; }
        public string EventDateText => EventDate.ToString("dddd, d MMMM yyyy");
        public DateTime EventTime { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string Organiser { get; set; }
        public string Email { get; set; }
        public string Facebook { get; set; }
        public bool IsNew { get; set; }
    }
}
