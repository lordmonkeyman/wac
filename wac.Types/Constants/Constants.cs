﻿namespace wac.Types.Constants
{
    public static class Constants
    {
        public const int AUTHENTICATION_EMAIL_ID = 1;
        public const string TIMESTAMP_FORMAT = "ffffHHMMyytssddmm";
        public const string CLIENTSIDE_DATETIME_FORMAT = "yyyyMMddHHmmss";
    }

    public static class Roles
    {
        public const string CONTENT_ADMINISTRATOR = "Content Administrator";
        public const string TRAINING_DIARY_ADMINISTRATOR = "Training Diary Administrator";
        public const string SOCIAL_DIARY_ADMINISTRATOR = "Social Diary Administrator";
    }

    public static class ContentTypes
    {
        public const int HTML = 1;
        public const int MARKDOWN = 2;
    }

    public static class EventTypeDescriptions
    {
        public const string TRAINING = "training";
        public const string SOCIAL = "social";
    }

    public static class ActionNames
    {
        public const string CREATE_CONTENT = "CreateContent";
        public const string UPDATE_CONTENT = "UpdateContent";
        public const string DELETE_CONTENT = "DeleteContent";
    }

    public static class FlashMessages
    {
        public const string CONTENT_CREATE_SUCCESSFUL = "Content created.";
        public const string CONTENT_CREATE_FAILED = "Something went wrong. Content could not be created.";
        public const string CONTENT_EDIT_SUCCESSFUL = "Changes have been saved.";
        public const string CONTENT_EDIT_FAILED = "Something went wrong. Changes could not be saved.";
        public const string CONTENT_VALIDATION_FAILED = "The following field(s) are required: ";
        public const string TRAININGDIARY_SAVE_FAILED = "Something went wrong. Changes could not be saved.";
        public const string TRAININGDIARY_SAVE_SUCCESSFUL = "Changes have been saved.";
        public const string SOCIALDIARY_SAVE_FAILED = "Something went wrong. Changes could not be saved.";
        public const string SOCIALDIARY_SAVE_SUCCESSFUL = "Changes have been saved.";
    }

    public static class FlashMessageCssClasses
    {
        public const string CONTENT_CREATE_SUCCESSFUL = "flash-message-successful";
        public const string CONTENT_CREATE_FAILED = "flash-message-failed";
        public const string CONTENT_EDIT_SUCCESSFUL = "flash-message-successful";
        public const string CONTENT_EDIT_FAILED = "flash-message-failed";
        public const string CONTENT_CREATE_WARNING = "flash-message-warning";
        public const string TRAININGDIARY_SAVE_FAILED = "flash-message-failed";
        public const string TRAININGDIARY_SAVE_SUCCESSFUL = "flash-message-successful";
        public const string SOCIALDIARY_SAVE_FAILED = "flash-message-failed";
        public const string SOCIALDIARY_SAVE_SUCCESSFUL = "flash-message-successful";
    }
}
