﻿namespace wac.Types
{
    public class Settings
    {
        public Database Database { get; set; }
        public string HostName { get; set; }
        public Authorisation Authorisation { get; set; }
        public Logging Logging { get; set; }
        public int ClubId { get; set; }
        public string Environment { get; set; }
        public string UploadDirectory { get; set; }
        public Caching Caching { get; set; }
    }

    public class Database
    {
        public string WacConnectionString { get; set; }
        public int ContentPageSize { get; set; }
        public int SocialDiaryListSize { get; set; }
    }

    public class Logging
    {
        public string LogPath { get; set; }
    }
    
    public class Authorisation
    {
        public string Scheme { get; set; } 
        public int ExpiryInMinutes { get; set; }
    }

    public class Caching {
        public bool UseCaching { get; set; }
        public int Expiration { get; set; }
    }
}
