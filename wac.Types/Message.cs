﻿using System;

namespace wac.Types
{
    public class Message
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public User User { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int BoardId { get; set; }
    }
}
