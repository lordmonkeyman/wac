﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wac.Types
{
    public class MessageBoard
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ClubId { get; set; }
        public string Description { get; set; }
        public List<Message> Messages { get; set; }
}
}
