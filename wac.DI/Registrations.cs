﻿using Microsoft.Extensions.DependencyInjection;
using wac.DataAccess.Access;
using wac.DataAccess.Club;
using wac.DataAccess.Content;
using wac.DataAccess.Emails;
using wac.DataAccess.Events;
using wac.DataAccess.MessageBoards;
using wac.DataAccess.Messages;
using wac.DataAccess.Upload;
using wac.Handlers.Access;
using wac.Handlers.Content;
using wac.Handlers.Events;
using wac.Handlers.Messages;
using wac.Handlers.StaticPage;
using wac.Handlers.Upload;
using wac.Interfaces.DataAccess;
using wac.Interfaces.DataAccess.Access;
using wac.Interfaces.DataAccess.Emails;
using wac.Interfaces.Handlers.Access;
using wac.Interfaces.Handlers.Content;
using wac.Interfaces.Handlers.Events;
using wac.Interfaces.Handlers.Messages;
using wac.Interfaces.Handlers.StaticPage;
using wac.Interfaces.Handlers.Upload;
using wac.Interfaces.Providers;
using wac.Interfaces.Providers.Events;
using wac.Providers;
using wac.Providers.Events;

namespace wac.DI
{
    public class Registrations
    {
        public Registrations(IServiceCollection services)
        {
            // Handlers
            services.AddScoped<IAuthenticationHandler, AuthenticationHandler>();
            services.AddScoped<IMessagesHandler, MessagesHandler>();
            services.AddScoped<IContentHandler, ContentHandler>();
            services.AddScoped<IUploadHander, UploadHandler>();
            services.AddScoped<ITrainingDiaryHandler, TrainingDiaryHandler>();
            services.AddScoped<ISocialDiaryHandler, SocialDiaryHandler>();
            services.AddScoped<IStaticPageHandler, StaticPageHandler>();

            // Scoped Providers
            services.AddScoped<IEmailProvider, MailgunProvider>();
            services.AddScoped<IAuthenticationProvider, AuthenticationProvider>();
            services.AddScoped<IMessagesProvider, MessagesProvider>();
            services.AddScoped<IMessageBoardsProvider, MessageBoardsProvider>();
            services.AddScoped<IContentProvider, ContentProvider>();
            services.AddScoped<IClubProvider, ClubProvider>();
            services.AddScoped<IDateTimeProvider, DateTimeProvider>();
            services.AddScoped<IMarkdownProvider, MarkdownProvider>();
            services.AddScoped<IUploadProvider, UploadProvider>();
            services.AddScoped<ITrainingDiaryProvider, TrainingDiaryProvider>();
            services.AddScoped<ISocialDiaryProvider, SocialDiaryProvider>();
            services.AddScoped<IStaticPageProvider, StaticPageProvider>();

            // Data Access
            services.AddScoped<IAuthenticationDataAccess, AuthenticationDataAccess>();
            services.AddScoped<IContentDataAccess, ContentDataAccess>();
            services.AddScoped<IMessagesDataAccess, MessagesDataAccess>();
            services.AddScoped<IEmailDataAccess, EmailDataAccess>();
            services.AddScoped<IMessageBoardsDataAccess, MessageBoardDataAccess>();
            services.AddScoped<IClubDataAccess, ClubDataAccess>();
            services.AddScoped<IUploadDataAccess, UploadDataAccess>();
            services.AddScoped<ITrainingDiaryDataAccess, TrainingDiaryDataAccess>();
            services.AddScoped<ISocialDiaryDataAccess, SocialDiaryDataAccess>();

            // Unique Providers
            services.AddSingleton<ISettingsProvider, SettingsProvider>();
            services.AddSingleton<ILogProvider, LogFileProvider>();
        }
    }
}
