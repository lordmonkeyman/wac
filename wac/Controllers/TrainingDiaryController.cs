﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using wac.Interfaces.Handlers.Events;
using wac.Interfaces.Providers;
using wac.Models.Events;
using wac.Types.Constants;

namespace wac.Controllers
{
    [Authorize(Roles = Roles.TRAINING_DIARY_ADMINISTRATOR)]
    public class TrainingDiaryController : Controller
    {
        private readonly ITrainingDiaryHandler _trainingDiaryHandler;
        private readonly IClubProvider _clubProvider;
        private readonly ILogProvider _logProvider;
        private readonly IDateTimeProvider _dateTimeProvider;

        public TrainingDiaryController(ITrainingDiaryHandler trainingDiaryHandler, IClubProvider clubProvider, ILogProvider logProvider, IDateTimeProvider dateTimeProvider)
        {
            _trainingDiaryHandler = trainingDiaryHandler;
            _clubProvider = clubProvider;
            _logProvider = logProvider;
            _dateTimeProvider = dateTimeProvider;
        }

        public IActionResult Index(int clubAreaId)
        {
            var vm = new TrainingDiaryViewModel();
            var weekCommencing = _dateTimeProvider.CalculateWeekCommencing(DateTime.Now, DayOfWeek.Monday);

            vm.ClubAreas = _trainingDiaryHandler.GetClubAreas();
            vm.CurrentClubArea = clubAreaId == 0 ? new Types.ClubArea { Id = 1, Description = "Main", Name = "Main" } : vm.ClubAreas.Find(x => x.Id == clubAreaId);
            vm.events = _trainingDiaryHandler.FetchTrainingDiary(weekCommencing);
            vm.WeekCommencing = weekCommencing;

            return View(vm);
        }

        public IActionResult NextWeek(int clubAreaId, string weekCommencing)
        {
            var vm = new TrainingDiaryViewModel();
            var nextWeekCommencing = new DateTime();
            if (DateTime.TryParseExact(weekCommencing, Constants.CLIENTSIDE_DATETIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out nextWeekCommencing))
            {
                nextWeekCommencing = nextWeekCommencing.AddDays(7);
                vm.ClubAreas = _trainingDiaryHandler.GetClubAreas();
                vm.CurrentClubArea = clubAreaId == 0 ? new Types.ClubArea { Id = 1, Description = "Main", Name = "Main" } : vm.ClubAreas.Find(x => x.Id == clubAreaId);
                vm.events = _trainingDiaryHandler.FetchTrainingDiary(nextWeekCommencing);
                vm.WeekCommencing = nextWeekCommencing;

                return View("Index", vm);
            }
            else {
                return null;
            }
        }

        public IActionResult PreviousWeek(int clubAreaId, string weekCommencing)
        {
            var vm = new TrainingDiaryViewModel();
            var previousWeekCommencing = new DateTime();
            if (DateTime.TryParseExact(weekCommencing, Constants.CLIENTSIDE_DATETIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out previousWeekCommencing))
            {
                previousWeekCommencing = previousWeekCommencing.AddDays(-7);
                vm.ClubAreas = _trainingDiaryHandler.GetClubAreas();
                vm.CurrentClubArea = clubAreaId == 0 ? new Types.ClubArea { Id = 1, Description = "Main", Name = "Main" } : vm.ClubAreas.Find(x => x.Id == clubAreaId);
                vm.events = _trainingDiaryHandler.FetchTrainingDiary(previousWeekCommencing);
                vm.WeekCommencing = previousWeekCommencing;

                return View("Index", vm);
            }
            else
            {
                return null;
            }
        }

        public IActionResult Edit(int id)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public IActionResult Create()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Types.Event trainingDiaryEvent)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([FromBody]List<Types.Event> diaryEvents)
        {
            //var vm = new TrainingDiaryViewModel();
            //vm.CurrentClubArea = new Types.ClubArea { Name = "TEST" };

            //List<Types.Event> events = _trainingDiaryHandler.Save(diaryEvents);

            //if (events.Count > 0)
            //{
            //    vm.events = events;
            //    vm.FlashMessage = new Types.FlashMessage { Message = FlashMessages.TRAININGDIARY_SAVE_SUCCESSFUL, State = Types.FlashMessageState.Successful, CssClass = FlashMessageCssClasses.TRAININGDIARY_SAVE_SUCCESSFUL };
            //    return View(vm);
            //}
            //else
            //{
            //    vm.events = events;
            //    vm.FlashMessage = new Types.FlashMessage { Message = FlashMessages.TRAININGDIARY_SAVE_FAILED, State = Types.FlashMessageState.Failed, CssClass = FlashMessageCssClasses.TRAININGDIARY_SAVE_FAILED };
            //    return View(vm);
            //}

            throw new NotImplementedException();
        }


    }
}