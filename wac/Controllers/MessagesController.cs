﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using wac.Interfaces.Handlers.Messages;
using wac.Models.Messages;

namespace wac.Controllers
{
    [Authorize]
    public class MessagesController : Controller
    {
        private readonly IMessagesHandler _messagesHandler;

        public MessagesController(IMessagesHandler messagesHandler)
        {
            _messagesHandler = messagesHandler;
        }

        public IActionResult Index(int id)
        {
            var vm = new GetMessageBoardViewModel();
            var board = _messagesHandler.GetMessageBoard(id);

            if (board != null)
            {
                vm.MessageBoard = board;
                return View(vm);
            }

            return new StatusCodeResult(404);
        }

        [HttpPost]
        public IActionResult Create(CreateMessageViewModel viewModel)
        {
            _messagesHandler.CreateNewMessage(viewModel.Title, viewModel.MessageText, 0, 1);

            return RedirectToAction("Index", "Messages");
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new CreateMessageViewModel());
        }
    }
}