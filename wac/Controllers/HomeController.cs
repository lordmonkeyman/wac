﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Diagnostics;
using wac.Interfaces.Handlers.Content;
using wac.Interfaces.Handlers.Events;
using wac.Interfaces.Providers;
using wac.Models;
using wac.Models.Events;
using wac.Models.Home;

namespace wac.Controllers
{
    public class HomeController : Controller
    {
        private readonly IContentHandler _contentHandler;
        private readonly ITrainingDiaryHandler _trainingDiaryHandler;
        private readonly ILogProvider _logProvider;
        private readonly ISocialDiaryHandler _socialDiaryHandler;

        public HomeController(
            IContentHandler contentHandler, 
            ITrainingDiaryHandler trainingDiaryHandler, 
            ILogProvider logProvider, 
            ISocialDiaryHandler socialDiaryHandler)
        {
            _contentHandler = contentHandler;
            _trainingDiaryHandler = trainingDiaryHandler;
            _logProvider = logProvider;
            _socialDiaryHandler = socialDiaryHandler;
        }

        public IActionResult Index()
        {
            var vm = new HomeViewModel();
            vm.FeaturedNews = _contentHandler.GetFeaturedContent();

            var weekCommencing = _trainingDiaryHandler.WeekCommencing();
            var canAdministerTrainingDiary = _trainingDiaryHandler.HasTrainingDiaryAdministratorRole();
            vm.TrainingDiaryViewModel = new TrainingDiaryViewModel
            {
                CanAdministerTrainingDiary = canAdministerTrainingDiary,
                WeekCommencing = weekCommencing,
                events = _trainingDiaryHandler.FetchTrainingDiaryForClubArea(1, weekCommencing)
            };

            vm.SocialDiaryViewModel = new SocialDiaryViewModel {
                SocialEvents = _socialDiaryHandler.FetchSocialDiaryForClubArea(1, true)
            };

            return View(vm);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult DataProtection()
        {
            return View();
        }

        public IActionResult Cookies()
        {
            return View();
        }

        [Authorize(Roles ="Administrator")]
        public IActionResult SecureArea()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Welcome to Weston Athletic Club";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            var statusCode = HttpContext.Response.StatusCode;
            var originalException = HttpContext.Features.Get<IExceptionHandlerFeature>();

            switch (statusCode)
            {
                case 404:
                    return RedirectToAction("PageDoesNotExist"); 
                default:
                    if (originalException != null)
                    {
                        try
                        {
                            _logProvider.Log($"EXCEPTION: {originalException.Error.Message}{Environment.NewLine}TRACE_IDENTIFIER: {HttpContext.TraceIdentifier}{Environment.NewLine}STACK: {originalException.Error.StackTrace}");
                        }
                        catch(Exception ex)
                        {
                            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
                        }
                    }
                    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            }
        }

        public IActionResult PageDoesNotExist()
        {
            return View("Custom404");

        }
    }
}
