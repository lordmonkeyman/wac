﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using wac.Interfaces.Handlers.Events;
using wac.Interfaces.Providers;
using wac.Types.Constants;

namespace wac.Controllers
{
    //[Route("api/[controller]")]
    [Produces("application/json")]
    public class SocialDiaryApiController : Controller
    {
        private readonly ISocialDiaryHandler _socialDiaryHandler;
        private readonly ILogProvider _logProvider;

        public SocialDiaryApiController(ISocialDiaryHandler socialDiaryHandler, ILogProvider logProvider)
        {
            _socialDiaryHandler = socialDiaryHandler;
            _logProvider = logProvider;
        }

        [HttpPost]
        [Authorize(Roles = Roles.SOCIAL_DIARY_ADMINISTRATOR)]
        [Route("api/SocialDiaryApi/DeleteEvent/")]
        [ValidateAntiForgeryToken]
        public bool DeleteEvent(int id)
        {
            return _socialDiaryHandler.DeleteEvent(id);
        }
    }
}