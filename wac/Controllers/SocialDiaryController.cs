﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using wac.Interfaces.Handlers.Events;
using wac.Interfaces.Providers;
using wac.Models.Events;
using wac.Types;
using wac.Types.Constants;

namespace wac.Controllers
{
    public class SocialDiaryController : Controller
    {
        private readonly ISocialDiaryHandler _socialDiaryHandler;
        private readonly IDateTimeProvider _dateTimeProvider;

        public SocialDiaryController(ISocialDiaryHandler socialDiaryHandler, IDateTimeProvider dateTimeProvider)
        {
            _socialDiaryHandler = socialDiaryHandler;
            _dateTimeProvider = dateTimeProvider;
        }

        public IActionResult Index()
        {
            var vm = new SocialDiaryViewModel();
            vm.CanAdministerSocialDiary = _socialDiaryHandler.HasSocialDiaryAdministratorRole();
            vm.SocialEvents = _socialDiaryHandler.FetchSocialDiaryForClubArea(1, false);

            return View(vm);
        }

        public IActionResult View(int id)
        {
            var vm = new SocialEventViewModel();
            vm.CanAdministerSocialDiary = _socialDiaryHandler.HasSocialDiaryAdministratorRole();
            vm.SocialEvent = _socialDiaryHandler.GetSocialEvent(id);

            return View(vm);
        }

        [Authorize(Roles = Roles.CONTENT_ADMINISTRATOR)]
        public IActionResult Create()
        {
            var vm = new SocialEventViewModel();
            vm.CanAdministerSocialDiary = _socialDiaryHandler.HasSocialDiaryAdministratorRole();
            vm.SocialEvent = new SocialEvent { EventDate = _dateTimeProvider.GetUtcNow(), IsNew = true };

            return View(vm);
        }

        [HttpPost]
        [Authorize(Roles = Roles.SOCIAL_DIARY_ADMINISTRATOR)]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SocialEvent socialEvent)
        {
            var vm = new SocialEventViewModel();
            vm.CanAdministerSocialDiary = _socialDiaryHandler.HasSocialDiaryAdministratorRole();
            vm.SocialEvent = _socialDiaryHandler.CreateSocialEvent(socialEvent);
            vm.FlashMessage = new FlashMessage { Message = string.Format(FlashMessages.SOCIALDIARY_SAVE_SUCCESSFUL), State = FlashMessageState.Successful, CssClass = FlashMessageCssClasses.SOCIALDIARY_SAVE_SUCCESSFUL };

            return View("Edit", vm);
        }

        [Authorize(Roles = Roles.CONTENT_ADMINISTRATOR)]
        public IActionResult Edit(int id)
        {
            var vm = new SocialEventViewModel
            {
                CanAdministerSocialDiary = _socialDiaryHandler.HasSocialDiaryAdministratorRole(),
                SocialEvent = _socialDiaryHandler.GetSocialEvent(id)
            };

            vm.SocialEvent.IsNew = false;

            return View(vm);
        }

        [HttpPost]
        [Authorize(Roles = Roles.SOCIAL_DIARY_ADMINISTRATOR)]
        [ValidateAntiForgeryToken]
        public IActionResult Save(SocialEvent socialEvent)
        {
            var vm = new SocialEventViewModel
            {
                SocialEvent = socialEvent,
                CanAdministerSocialDiary = _socialDiaryHandler.HasSocialDiaryAdministratorRole(),
            };

            vm.SocialEvent.ClubArea = new ClubArea { Id = 1 };

            if (_socialDiaryHandler.UpdateSocialEvent(socialEvent))
            {
                vm.SocialEvent.IsNew = false;
                vm.FlashMessage = new FlashMessage { Message = string.Format(FlashMessages.SOCIALDIARY_SAVE_SUCCESSFUL), State = FlashMessageState.Successful, CssClass = FlashMessageCssClasses.SOCIALDIARY_SAVE_SUCCESSFUL };
                return View("Edit", vm);
            }

            vm.SocialEvent.IsNew = false;
            vm.FlashMessage = new FlashMessage { Message = string.Format(FlashMessages.SOCIALDIARY_SAVE_FAILED), State = FlashMessageState.Failed, CssClass = FlashMessageCssClasses.SOCIALDIARY_SAVE_FAILED };
            return View("Edit", vm);
        }

        [HttpPost]
        [Authorize(Roles = Roles.SOCIAL_DIARY_ADMINISTRATOR)]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteEvent(SocialEventViewModel socialEventVm)
        {
            _socialDiaryHandler.DeleteEvent(socialEventVm.SocialEvent.Id);

            var vm = new SocialDiaryViewModel();
            vm.CanAdministerSocialDiary = _socialDiaryHandler.HasSocialDiaryAdministratorRole();
            vm.SocialEvents = _socialDiaryHandler.FetchSocialDiaryForClubArea(1, false);

            return View("Index", vm);
        }
    }
}