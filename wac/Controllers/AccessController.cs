﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using wac.Interfaces.Providers;
using wac.Models.Access;
using wac.Types;

namespace wac.Controllers
{
    public class AccessController : Controller
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly Settings _settings;
        private readonly Interfaces.Handlers.Access.IAuthenticationHandler _authenticationHandler;
        private readonly ILogProvider _logProvider;

        public AccessController(ISettingsProvider settingsProvider, Interfaces.Handlers.Access.IAuthenticationHandler authenticationHandler, ILogProvider logProvider)
        {
            _settingsProvider = settingsProvider;
            _settings = settingsProvider.Get();
            _authenticationHandler = authenticationHandler;
            _logProvider = logProvider;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel viewModel)
        {
            _authenticationHandler.RequestAuthentication(viewModel.LoginRequest);
                
            return View("EmailSent");
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(scheme: _settings.Authorisation.Scheme);
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public async Task<IActionResult> Authenticate(string token)
        {
            User user;
            if (_authenticationHandler.ValidateToken(token, out user))
            {
                HttpContext.Session.SetString("FirstName", user.FirstName);

                List<Claim> roleClaims = user.Roles.ConvertAll(x => new Claim(ClaimTypes.Role, x.Name));

                List<Claim> claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, token),
                };
                claims.AddRange(roleClaims);

                ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                
                await HttpContext.SignInAsync(
                        scheme: _settings.Authorisation.Scheme,
                        principal: principal);

                return RedirectToAction("Index", "Home");
            }

            return View("Custom401");
        }
    }
}