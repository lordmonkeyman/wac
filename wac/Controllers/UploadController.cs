﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using wac.Interfaces.Handlers.Upload;
using wac.Models.Upload;
using wac.Types.Constants;

namespace wac.Controllers
{
    public class UploadController : Controller
    {
        private readonly IUploadHander _uploadHandler;

        public UploadController(IUploadHander uploadHandler)
        {
            _uploadHandler = uploadHandler;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = Roles.CONTENT_ADMINISTRATOR)]
        public IActionResult List()
        {
            var vm = new ListUploadsViewModel {
                Uploads = _uploadHandler.GetUploads()
            };
            return View(vm);
        }

        [HttpPost]
        [Authorize(Roles = Roles.CONTENT_ADMINISTRATOR)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadFiles(IFormFile file)
        {
            await _uploadHandler.Upload(file);
            return RedirectToAction("Index");
        }
    }
}