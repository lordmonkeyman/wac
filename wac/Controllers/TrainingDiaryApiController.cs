﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using wac.Interfaces.Handlers.Events;
using wac.Interfaces.Providers;
using wac.Models.Content;
using wac.Models.Events;
using wac.Types.Constants;

namespace wac.Controllers
{
    [Produces("application/json")]
    //[ApiController]
    public class TrainingDiaryApiController : Controller//Base
    {
        private readonly ITrainingDiaryHandler _trainingDiaryHandler;
        private readonly IClubProvider _clubProvider;
        private readonly ILogProvider _logProvider;

        public TrainingDiaryApiController(ITrainingDiaryHandler trainingDiaryHandler, IClubProvider clubProvider, ILogProvider logProvider)
        {
            _trainingDiaryHandler = trainingDiaryHandler;
            _clubProvider = clubProvider;
            _logProvider = logProvider;
        }

        [HttpGet]
        [Route("api/TrainingDiaryApi/FetchTrainingDiaryForClubArea/{clubAreaId}/{weekCommencing}")]
        public HomeTrainingDiaryViewModel Index(int clubAreaId, string weekCommencing)
        {
            DateTime weekCommencingDate;
            if (DateTime.TryParseExact(weekCommencing, Constants.CLIENTSIDE_DATETIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out weekCommencingDate))
            {
                var vm = new HomeTrainingDiaryViewModel
                {
                    TrainingDiary = _trainingDiaryHandler.FetchTrainingDiaryForClubArea(clubAreaId, weekCommencingDate),
                    CanAdministerTrainingDiary = _trainingDiaryHandler.HasTrainingDiaryAdministratorRole()
                };

                return vm;
            }

            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Roles.TRAINING_DIARY_ADMINISTRATOR)]
        [Route("api/TrainingDiaryApi/Save")]
        public TrainingDiaryViewModel Save(List<Types.Event> diaryEvents)
        {
            var vm = new TrainingDiaryViewModel();
            List<Types.Event> events = _trainingDiaryHandler.Save(diaryEvents);
            
            if (events.Count > 0)
            {
                vm.events = events;
                vm.FlashMessage = new Types.FlashMessage { Message = FlashMessages.TRAININGDIARY_SAVE_SUCCESSFUL, State = Types.FlashMessageState.Successful, CssClass = FlashMessageCssClasses.TRAININGDIARY_SAVE_SUCCESSFUL };
                return vm;
            }
            else
            {
                vm.events = events;
                vm.FlashMessage = new Types.FlashMessage { Message = FlashMessages.TRAININGDIARY_SAVE_FAILED, State = Types.FlashMessageState.Failed, CssClass = FlashMessageCssClasses.TRAININGDIARY_SAVE_FAILED };
                return vm;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Roles.TRAINING_DIARY_ADMINISTRATOR)]
        [Route("api/TrainingDiaryApi/ChangeTrainingDiaryArea")]
        public TrainingDiaryViewModel ChangeTrainingDiaryArea(int clubAreaId, string weekCommencing)
        {
            var vm = new TrainingDiaryViewModel();
            var wkCommencing = new DateTime();
            if (DateTime.TryParseExact(weekCommencing, Constants.CLIENTSIDE_DATETIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out wkCommencing))
            {
                vm.ClubAreas = _trainingDiaryHandler.GetClubAreas();
                vm.CurrentClubArea = vm.ClubAreas.Find(x => x.Id == clubAreaId);
                vm.events = _trainingDiaryHandler.FetchTrainingDiaryForClubArea(clubAreaId, wkCommencing);
                vm.WeekCommencing = wkCommencing;

                return vm;
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Roles.TRAINING_DIARY_ADMINISTRATOR)]
        [Route("api/TrainingDiaryApi/NextWeek")]
        public TrainingDiaryViewModel NextWeek(int clubAreaId, string weekCommencing)
        {
            var vm = new TrainingDiaryViewModel();
            var nextWeekCommencing = new DateTime();
            if (DateTime.TryParseExact(weekCommencing, Constants.CLIENTSIDE_DATETIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out nextWeekCommencing))
            {
                nextWeekCommencing = nextWeekCommencing.AddDays(7);
                vm.ClubAreas = _trainingDiaryHandler.GetClubAreas();
                vm.CurrentClubArea = clubAreaId == 0 ? new Types.ClubArea { Id = 1, Description = "Main", Name = "Main" } : vm.ClubAreas.Find(x => x.Id == clubAreaId);
                vm.events = _trainingDiaryHandler.FetchTrainingDiaryForClubArea(clubAreaId, nextWeekCommencing);
                vm.WeekCommencing = nextWeekCommencing;

                return vm;
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = Roles.TRAINING_DIARY_ADMINISTRATOR)]
        [Route("api/TrainingDiaryApi/PreviousWeek")]
        public TrainingDiaryViewModel PreviousWeek(int clubAreaId, string weekCommencing)
        {
            var vm = new TrainingDiaryViewModel();
            var previousWeekCommencing = new DateTime();
            if (DateTime.TryParseExact(weekCommencing, Constants.CLIENTSIDE_DATETIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out previousWeekCommencing))
            {
                previousWeekCommencing = previousWeekCommencing.AddDays(-7);
                vm.ClubAreas = _trainingDiaryHandler.GetClubAreas();
                vm.CurrentClubArea = clubAreaId == 0 ? new Types.ClubArea { Id = 1, Description = "Main", Name = "Main" } : vm.ClubAreas.Find(x => x.Id == clubAreaId);
                vm.events = _trainingDiaryHandler.FetchTrainingDiaryForClubArea(clubAreaId, previousWeekCommencing);
                vm.WeekCommencing = previousWeekCommencing;

                return vm;
            }
            else
            {
                return null;
            }
        }
    }
}