﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using wac.Handlers.Upload;
using wac.Interfaces.Handlers.Upload;
using wac.Types.Constants;

namespace wac.Controllers
{
    [Produces("application/json")]
    [Route("Upload/api/UploadApi")]
    public class UploadApiController : Controller
    {
        private readonly IUploadHander _uploadHandler;

        public UploadApiController(IUploadHander uploadHandler)
        {
            _uploadHandler = uploadHandler;
        }

        [HttpPost]
        [Authorize(Roles = Roles.CONTENT_ADMINISTRATOR)]
        [ValidateAntiForgeryToken]
        public bool DeleteUpload(int id)
        {
            return _uploadHandler.DeleteUpload(id);
        }
    }
}