﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.IO;
using wac.Interfaces.Handlers.StaticPage;
using wac.Models.StaticPage;

namespace wac.Controllers
{
    public class StaticPageController : Controller
    {
        private readonly IStaticPageHandler _staticPageHandler;
        
        public StaticPageController(IStaticPageHandler staticPageHandler)
        {
            _staticPageHandler = staticPageHandler;
        }

        [Route("staticpage/{folder}/{pagename}")]
        public IActionResult Index(string folder, string pagename)
        {
            StaticPageViewModel vm = new StaticPageViewModel();

            try
            {
                vm.Content = _staticPageHandler.GetContent(folder, pagename);
            }
            catch(Exception ex)
            {
                if (ex is FileNotFoundException)
                {
                    return NotFound();
                }

                throw (ex);
            }

            return View("StaticPage", vm);
        }
    }
}