﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using wac.Interfaces.Handlers.Content;
using wac.Interfaces.Providers;
using wac.Types;
using wac.Types.Constants;

namespace wac.Controllers
{
    [Produces("application/json")]
    //[Route("api/ContentApi")]
    public class ContentApiController : Controller
    {
        private readonly IContentHandler _contentHandler;
        private readonly ILogProvider _logProvider;

        public ContentApiController(IContentHandler contentHandler, ILogProvider logProvider)
        {
            _contentHandler = contentHandler;
            _logProvider = logProvider;
        }
        
        [HttpPost]
        [Authorize(Roles = Roles.CONTENT_ADMINISTRATOR)]
        [Route("api/ContentApi/DeleteContent/")]
        [ValidateAntiForgeryToken]
        public bool DeleteContent(int id)
        {
            return _contentHandler.DeleteContent(id);
        }

        [HttpGet]
        [Route("api/ContentApi/FetchContentForClubArea/")]
        public IEnumerable<Content> FetchContentForClubArea(int clubAreaId, string from, bool featuredOnly)
        {
            var canAdministerContent = _contentHandler.HasContentAdministratorRole();
            var activeOnly = !canAdministerContent;

            DateTime fromDate;
            if (DateTime.TryParseExact(from, Constants.CLIENTSIDE_DATETIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out fromDate))
            {
                return _contentHandler.FetchContentForClubArea(clubAreaId, fromDate, activeOnly, featuredOnly);
            }

            return null;
        }

        [HttpGet]
        [Route("api/ContentApi/SearchContent/")]
        public IEnumerable<Content> SearchContent(string searchTerm)
        {
            var canAdministerContent = _contentHandler.HasContentAdministratorRole();
            var activeOnly = !canAdministerContent;

            return _contentHandler.SearchContent(activeOnly, searchTerm);
        }

    }
}