﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using wac.Interfaces.Handlers.Content;
using wac.Models.Content;
using wac.Types.Constants;
using wac.Interfaces.Providers;
using Microsoft.AspNetCore.Authorization;

namespace wac.Controllers
{
    public class ContentController : Controller
    {
        private readonly IContentHandler _contentHandler;
        private readonly IClubProvider _clubProvider;
        private readonly ILogProvider _logProvider;

        public ContentController(IContentHandler contentHandler, IClubProvider clubProvider, ILogProvider logProvider)
        {
            _contentHandler = contentHandler;
            _clubProvider = clubProvider;
            _logProvider = logProvider;
        }

        [Authorize(Roles = Roles.CONTENT_ADMINISTRATOR)]
        public IActionResult Edit(int id)
        {
            var vm = new EditContentViewModel {
                Content = _contentHandler.GetContent(id)
            };
            vm.Content.ClubAreas = _clubProvider.GetClubAreas();
            return View(vm);
        }

        [HttpGet]
        [Authorize(Roles = Roles.CONTENT_ADMINISTRATOR)]
        public IActionResult Create()
        {
            var vm = new CreateContentViewModel {
                Content = new Types.Content
                {
                    Title = "New content",
                    PublishDateTime = DateTime.Now,
                    ClubArea = new Types.ClubArea { Id = 0 },
                    ClubAreas = _clubProvider.GetClubAreas(),
                    IsNew = true
                }
            };

            return View(vm);
        }

        [HttpPost]
        [Authorize(Roles = Roles.CONTENT_ADMINISTRATOR)]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Types.Content content)
        {
            var vm = new EditContentViewModel();

            vm.Content = _contentHandler.CreateContent(content);
            vm.Content.IsNew = false;
            vm.Content.ClubAreas = _clubProvider.GetClubAreas();
            vm.FlashMessage = new Types.FlashMessage { Message = string.Format(FlashMessages.CONTENT_CREATE_SUCCESSFUL, content.Title), State = Types.FlashMessageState.Successful, CssClass = FlashMessageCssClasses.CONTENT_CREATE_SUCCESSFUL };
            
            return View("Edit", vm);
        }

        [HttpPost]
        [Authorize(Roles = Roles.CONTENT_ADMINISTRATOR)]
        [ValidateAntiForgeryToken]
        public IActionResult Save(Types.Content content)
        {
            var vm = new EditContentViewModel { Content = content };

            if (_contentHandler.UpdateContent(content))
            {
                vm.Content.ClubAreas = _clubProvider.GetClubAreas();
                vm.Content.IsNew = false;
                vm.FlashMessage = new Types.FlashMessage { Message = string.Format(FlashMessages.CONTENT_EDIT_SUCCESSFUL, content.Title), State = Types.FlashMessageState.Successful, CssClass = FlashMessageCssClasses.CONTENT_EDIT_SUCCESSFUL };
                return View("Edit", vm);
            }

            vm.Content.ClubAreas = _clubProvider.GetClubAreas();
            vm.Content.IsNew = false;
            vm.FlashMessage = new Types.FlashMessage { Message = string.Format(FlashMessages.CONTENT_EDIT_FAILED, content.Title), State = Types.FlashMessageState.Failed, CssClass = FlashMessageCssClasses.CONTENT_EDIT_FAILED };
            return View("Edit", vm);
        }

        public IActionResult Index(int clubAreaId)
        {
            var vm = new ContentViewModel();
            var canAdministerContent = _contentHandler.HasContentAdministratorRole();

            vm.PagedNews = _contentHandler.FetchContentForClubArea(clubAreaId, DateTime.MaxValue, !canAdministerContent, false);
            vm.ClubAreas = _contentHandler.GetClubAreas();
            vm.CurrentClubArea =  clubAreaId == 0 ? new Types.ClubArea { Id = 0, Description = "All Weston AC news", Name = "All news" }  : vm.ClubAreas.Find(x => x.Id == clubAreaId);
            vm.ClubAreaId = clubAreaId;
            vm.OldestNewsDateTime = vm.PagedNews.Count > 0 ? vm.PagedNews.OrderBy(x => x.PublishDateTime).FirstOrDefault().PublishDateTime : DateTime.Now;
            vm.CanAdministerContent = canAdministerContent;
            
            return View(vm);
        }
    }
}