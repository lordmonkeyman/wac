﻿using Microsoft.AspNetCore.Mvc;
using wac.Models.Messages;
using wac.Interfaces.Handlers.Messages;
using Microsoft.AspNetCore.Authorization;

namespace wac.Controllers
{
    [Authorize]
    public class MessageBoardsController : Controller
    {
        private readonly IMessagesHandler _messagesHandler;

        public MessageBoardsController(IMessagesHandler messagesHandler)
        {
            _messagesHandler = messagesHandler;
        }

        public IActionResult Index()
        {
            var vm = new GetAllMessageBoardsViewModel();
            vm.MessageBoards = _messagesHandler.GetAllMessageBoards();

            return View(vm);
        }
    }
}