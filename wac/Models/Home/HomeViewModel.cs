﻿using System.Collections.Generic;
using wac.Models.Events;

namespace wac.Models.Home
{
    public class HomeViewModel
    {
        public List<Types.Content> FeaturedNews { get; set; }
        public TrainingDiaryViewModel TrainingDiaryViewModel { get; set; }
        public SocialDiaryViewModel SocialDiaryViewModel { get; set; }
    }
}
