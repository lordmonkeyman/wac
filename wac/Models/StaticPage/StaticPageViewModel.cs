﻿namespace wac.Models.StaticPage
{
    public class StaticPageViewModel
    {
        public string Content { get; set; }
    }
}
