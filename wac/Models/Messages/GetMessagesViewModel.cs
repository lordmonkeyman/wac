﻿using wac.Types;
using System.Collections.Generic;

namespace wac.Models.Messages
{
    public class GetMessagesViewModel
    {
        public List<Message> Messages { get; set; }
    }
}
