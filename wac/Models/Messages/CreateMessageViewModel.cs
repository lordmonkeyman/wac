﻿using wac.Types;

namespace wac.Models.Messages
{
    public class CreateMessageViewModel
    {
        public string MessageText { get; set; }
        public string Title { get; set; }
    }
}
