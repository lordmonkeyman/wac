﻿using wac.Types;

namespace wac.Models.Messages
{
    public class GetMessageBoardViewModel
    {
        public MessageBoard MessageBoard { get; set; }
    }
}
