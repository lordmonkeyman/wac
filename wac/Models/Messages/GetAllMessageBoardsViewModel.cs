﻿using wac.Types;
using System.Collections.Generic;

namespace wac.Models.Messages
{
    public class GetAllMessageBoardsViewModel
    {
        public List<MessageBoard> MessageBoards { get; set; }
    }
}
