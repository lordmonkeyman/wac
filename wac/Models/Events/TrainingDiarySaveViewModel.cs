﻿using System.Collections.Generic;

namespace wac.Models.Events
{
    public class TrainingDiarySaveViewModel
    {
        public List<Types.Event> events { get; set; }

    }
}
