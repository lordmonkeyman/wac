﻿using System.Collections.Generic;
using wac.Types;

namespace wac.Models.Events
{
    public class SocialDiaryViewModel
    {
        public List<SocialEvent> SocialEvents { get; set; }
        public bool CanAdministerSocialDiary { get; set; }
        public ClubArea CurrentClubArea { get; set; }
        public FlashMessage FlashMessage { get; set; }
        public List<ClubArea> ClubAreas { get; set; }
    }
}
