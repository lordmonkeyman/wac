﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wac.Types;
using wac.Types.Constants;

namespace wac.Models.Events
{
    public class TrainingDiaryViewModel
    {
        public List<wac.Types.Event> events { get; set; }
        public DateTime WeekCommencing { get; set; }
        public string WeekCommencingFormatted => WeekCommencing.ToString(Constants.CLIENTSIDE_DATETIME_FORMAT);
        public string WeekCommencingText => WeekCommencing.ToString("dddd, d MMMM yyyy");
        public bool CanAdministerTrainingDiary { get; set; }
        public ClubArea CurrentClubArea { get; set; }
        public FlashMessage FlashMessage { get; set; }
        public List<ClubArea> ClubAreas { get; set; }
    }
}
