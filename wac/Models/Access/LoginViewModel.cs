﻿using System;
using wac.Types;

using wac.Types.Constants;

namespace wac.Models.Access
{
    public class LoginViewModel
    {
        public LoginViewModel()
        {
            LoginRequest = new LoginRequest { TimeStamp = DateTime.UtcNow.ToString(Constants.TIMESTAMP_FORMAT) };
        }

        public LoginRequest LoginRequest { get; set; }
    }
}

