﻿using System.Collections.Generic;
using wac.Types;

namespace wac.Models.Upload
{
    public class ListUploadsViewModel
    {
        public List<Types.Upload> Uploads { get; set; }
    }
}
