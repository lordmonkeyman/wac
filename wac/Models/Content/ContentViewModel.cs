﻿using System;
using System.Collections.Generic;
using wac.Types;

namespace wac.Models.Content
{
    public class ContentViewModel
    {
        public List<ClubArea> ClubAreas { get; set; }
        public List<Types.Content> PagedNews { get; set; }
        public int ClubAreaId { get; set; }
        public ClubArea CurrentClubArea { get; set; }
        public DateTime OldestNewsDateTime { get; set; }
        public bool CanAdministerContent { get; set; }
    }
}
