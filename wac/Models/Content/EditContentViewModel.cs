﻿using System.Collections.Generic;
using wac.Types;

namespace wac.Models.Content
{
    public class EditContentViewModel
    {
        public Types.Content Content {get; set;}  
        public FlashMessage FlashMessage { get; set; }
    }
}
