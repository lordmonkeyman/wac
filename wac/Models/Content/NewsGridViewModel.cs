﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace wac.Models.Content
{
    public class NewsGridViewModel
    {
        public List<News> NewsList { get; set; }

        public class News
        {
            public string Title { get; set; }
            public string Body { get; set; }
            public DateTime PublishedDateTime { get; set; }
        }
    }


}
