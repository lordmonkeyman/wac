﻿using System;
using System.Collections.Generic;
using wac.Types;

namespace wac.Models.Content
{
    public class HomeTrainingDiaryViewModel
    {
        public List<Event> TrainingDiary { get; set; }
        public bool CanAdministerTrainingDiary { get; set; }
    }
}
