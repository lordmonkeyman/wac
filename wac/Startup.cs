﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using wac.DI;
using wac.Types;

namespace wac
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            new Registrations(services);

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            var authorisationScheme = Configuration["Authorisation:Scheme"];
            var expiryInMinutes = Convert.ToInt32(Configuration["Authorisation:ExpiryInMinutes"]);
            services.AddAuthentication(authorisationScheme)
                    .AddCookie(authorisationScheme, options =>
                    {
                        options.AccessDeniedPath = new PathString("/Access/401");
                        options.LoginPath = new PathString("/Access/Login");
                        options.ExpireTimeSpan = new System.TimeSpan(0, expiryInMinutes, 0);
                    });

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDistributedMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            var environment = Configuration.Get<Settings>().Environment;
            env.EnvironmentName = environment;

            if (env.IsDevelopment())
            {
                //app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseStatusCodePagesWithReExecute("/Home/error/{0}");
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseSession();

            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();
            //// loggerFactory.AddFile("c:/test.log");

            //var logger = loggerFactory.CreateLogger("Test Logging");
            //logger.LogInformation("HELLO LOGGING");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}");
                routes.MapRoute(
                    name: "Children",
                    template: "{controller=Home}/{action=Index}/{id}");
                routes.MapRoute(name: "access",
                    template: "{controller=Access}/{action=Index}");
                routes.MapRoute(name: "Edit",
                    template: "{controller=Access}/{action=Index}/{clubArea}/{id}");
                routes.MapRoute(name: "AddNew",
                    template: "{controller=Access}/{action=Index}/{clubArea}");
            });
        }
    }
}
