﻿const uri = 'api/ContentApi';
const requestVerificationToken = $('input[name=__RequestVerificationToken]').val();

$(document).ready(function () {
    
});

function ConfirmDialog(message, title, yesFunction, id) {
    // https://api.jqueryui.com/dialog/
    $('<div></div>').appendTo('body')
        .html('<div><h6 class="ui-dialog-content-custom">' + message + '</h6></div>')
        .dialog({
            classes: {},
            modal: true, title: title, zIndex: 10000, autoOpen: true,
            width: 300, resizable: false,
            buttons: {
                Yes: function () {
                    yesFunction(id);
                    $(this).dialog("close");
                },
                No: function () {
                    $(this).dialog("close");
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });
}

function deleteContent(id) {
    console.log("ID:" + id);
    $.ajax({
        type: 'POST',
        url: uri + '/DeleteContent',
        data: { __RequestVerificationToken: requestVerificationToken, id },
        success: function (data) {
            var content = $('#aside-content-' + id);
            content.hide('slow', function () {
                content.remove();
            });
        }
    });
}

function buildAdminFragment(contentId) {
    return '<div class="mini-menu-float-none mini-menu-dropdown"><ul><li>' +
        '<a title="DeleteContent" id="delete-content" onclick="ConfirmDialog(&#39;Delete content: New content?&#39;, &#39;Delete&#39;, deleteContent, ' + contentId + ')">Delete content</a>' +
        '</li><li>' +
        '<a title="EditContent" id="edit-content" href="/Content/edit?Id=' + contentId + '">Edit content</a>' +
        '</li></ul></div>';
}

function getData(isAdmin) {
    var clubAreaId = $('#club-area-id').text();
    var from = new Date($('#oldest-news-datetime').text());
    from = formatRequestDate(from, false);
    var featuredOnly = $('#featured-only').prop("checked");
    var url = uri + '/FetchContentForClubArea?clubAreaId=' + clubAreaId + '&from=' + from + '&featuredOnly=' + featuredOnly;
    getDataFromApi(isAdmin, url);
}

function toggleFeatured(isAdmin) {
    var clubAreaId = $('#club-area-id').text();
    var featuredOnly = $('#featured-only').prop("checked");
    var from = new Date(Date.now());
    from = formatRequestDate(from, true);
    var url = uri + '/FetchContentForClubArea?clubAreaId=' + clubAreaId + '&from=' + from + '&featuredOnly=' + featuredOnly;
    getDataFromApi(isAdmin, url, true);
}

function formatRequestDate(requestDate, addEndOfDay) {
    if (addEndOfDay) return requestDate.getFullYear().toString() + bufferNumberString((requestDate.getMonth() + 1).toString()) + bufferNumberString(requestDate.getDate().toString()) + '115959';
    return requestDate.getFullYear().toString() + bufferNumberString((requestDate.getMonth() + 1).toString()) + bufferNumberString(requestDate.getDate().toString()) + bufferNumberString(requestDate.getHours().toString()) + bufferNumberString(requestDate.getMinutes().toString()) + bufferNumberString(requestDate.getSeconds().toString());
}

function doSearch(isAdmin) {
    var searchTerm = $('#search-term').val();
    var url = uri + '/SearchContent?&searchTerm=' + searchTerm;
    getDataFromApi(isAdmin, url, true);
}

function getDataFromApi(isAdmin, url, clearExistingContent) {
    if (clearExistingContent) clearContent();

    $.ajax({
        type: 'GET',
        url: url,
        success: function (data) {
            $.each(data, function (key, item) {
                var adminFragment = isAdmin ? buildAdminFragment(item.id) : '';
                $('<aside id="aside-content-' + item.id + '">' +
                    '<div class="content">' +
                    '<h3>' + item.title + '</h3>' +
                    '<h5>' + item.publishDateTimeText + '</h5>' +
                    adminFragment + 
                    item.body +
                    '</div>' +
                    '</aside>').appendTo($('#content'));
            });

            if (data && data.length > 0) $('#oldest-news-datetime').text(oldestNewsDateTime(data));
        }
    });
}

function oldestNewsDateTime(data) {
    if (data.length > 0) {
        return data[data.length - 1].publishDateTime;
    }
    return '';
}

function clearContent() {
    $('#content').empty();
}
