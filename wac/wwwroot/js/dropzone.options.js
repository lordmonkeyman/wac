﻿Dropzone.options.uploader = {
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 2, // MB
    clickable: true,
    acceptedFiles: "image/jpeg,image/png,image/gif,application/pdf",
    createImageThumbnails: true,
    dictDefaultMessage: "Drag files onto box to upload or click here to select",
    dictFallbackMessage: "Sorry your browser does not support file uploads",
    dictInvalidFileType: "Only images and pdfs can be uploaded",
    dictFileTooBig: "Files must be smaller than 2mb"
};