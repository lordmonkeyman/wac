﻿const uri = 'api/UploadApi';
const requestVerificationToken = $('input[name=__RequestVerificationToken]').val();

function deleteUpload(id) {
    $.ajax({
        type: 'POST',
        url: uri,
        data: { __RequestVerificationToken: requestVerificationToken, id },
        success: function (data) {
            var uploadDiv = $('#upload-' + id);
            uploadDiv.hide('slow', function () {
                uploadDiv.remove();
            });
        }
    });
}