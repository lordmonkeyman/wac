﻿const uri = 'api/TrainingDiaryApi';

$(function () {
    $("#accordion").accordion({
        collapsible: true
    });
});

function saveTrainingDiary() {
    var postData = validateAndBindSave();
    $.ajax({
        type: 'POST',
        url: 'api/TrainingDiaryApi/Save',
        data: postData,
        success: function (data) {
            rebuildPage(data, true);
        }
    });
}

function changeTrainingDiaryArea(clubAreaId) {
    var requestVerificationToken = $('input[name=__RequestVerificationToken]').val();
    const weekCommencing = $('#WeekCommencingFormatted').val();
    var postData = {
        __RequestVerificationToken: requestVerificationToken,
        clubAreaId: clubAreaId,
        weekCommencing: weekCommencing
    };

    $.ajax({
        type: 'POST',
        url: 'api/TrainingDiaryApi/ChangeTrainingDiaryArea',
        data: postData,
        success: function (data) {
            rebuildPage(data, false);
        }
    });
}

function nextWeek() {
    // TODO - unsaved changes?
    const clubAreaId = $('#CurrentClubArea_Id').val();
    const weekCommencing = $('#WeekCommencingFormatted').val();
    const requestVerificationToken = $('input[name=__RequestVerificationToken]').val();
    const postData = { __RequestVerificationToken: requestVerificationToken, clubAreaId, weekCommencing };
    $.ajax({
        type: 'POST',
        url: 'api/TrainingDiaryApi/NextWeek',
        data: postData,
        success: function (data) {
            rebuildPage(data, false);
        }
    });
}

function previousWeek() {
    // TODO - unsaved changes?
    const clubAreaId = $('#CurrentClubArea_Id').val();
    const weekCommencing = $('#WeekCommencingFormatted').val();
    const requestVerificationToken = $('input[name=__RequestVerificationToken]').val();
    const postData = { __RequestVerificationToken: requestVerificationToken, clubAreaId, weekCommencing };
    $.ajax({
        type: 'POST',
        url: 'api/TrainingDiaryApi/PreviousWeek',
        data: postData,
        success: function (data) {
            rebuildPage(data, false);
        }
    });
}

function rebuildPage(vm, preserveNonEventData) {

    if (!preserveNonEventData) {
        const currentClubAreaIdControl = $('#CurrentClubArea_Id');
        const weekCommencingFormattedControl = $('#WeekCommencingFormatted');
        const weekCommencingTextControl = $('#week-commencing-text');
        const headerTextControl = $('#header-text');

        currentClubAreaIdControl.val(vm.currentClubArea.id);
        weekCommencingFormattedControl.val(vm.weekCommencingFormatted);
        weekCommencingTextControl.text(`Week commencing: ${vm.weekCommencingText}`);
        headerTextControl.text(`Training Diary - ${vm.currentClubArea.name}`);
    }

    for (var idx = 0; idx < 7; idx++) {

        const idControl = $('#events_' + idx + '__Id');
        const clubAreaIdControl = $('#events_' + idx + '__ClubArea_Id');
        const locationControl = $('#events_' + idx + '__Location');
        const descriptionControl = $('#events_' + idx + '__Description');
        const eventDateControl = $('#events_' + idx + '__EventDate');
        const startTimeControl = $('#events_' + idx + '__StartTime');
        const endTimeControl = $('#events_' + idx + '__EndTime');

        idControl.val(vm.events[idx].id);
        clubAreaIdControl.val(vm.events[idx].clubArea.id);
        locationControl.val(vm.events[idx].location);
        descriptionControl.val(vm.events[idx].description);
        eventDateControl.val(vm.events[idx].eventDate);
        startTimeControl.val(formatDateTimeToTime(vm.events[idx].startTime));
        endTimeControl.val(formatDateTimeToTime(vm.events[idx].endTime));
    }

    if (vm.flashMessage) {
        rebuildFlashMessage(vm.flashMessage);
    } else {
        clearFlashMessage();
    }
}

function clearFlashMessage() {
    const divFlashMessage = $('#flash-message');
    divFlashMessage.empty();
    divFlashMessage.removeClass();
    divFlashMessage.addClass('flash-message');
}

function rebuildFlashMessage(flashMessage) {
    const divFlashMessage = $('#flash-message');
    divFlashMessage.text(flashMessage.message);
    divFlashMessage.addClass(flashMessage.cssClass);
}

function validateAndBindSave() {
    var requestVerificationToken = $('input[name=__RequestVerificationToken]').val();
    var vm = { __RequestVerificationToken: requestVerificationToken, diaryEvents: [] };
    
    for (var idx = 0; idx < 7; idx++) {

        const idControl = $('#events_' + idx + '__Id');
        const clubAreaIdControl = $('#events_' + idx + '__ClubArea_Id');
        const locationControl = $('#events_' + idx + '__Location');
        const descriptionControl = $('#events_' + idx + '__Description');
        const eventDateControl = $('#events_' + idx + '__EventDate');
        const startTimeControl = $('#events_' + idx + '__StartTime');
        const endTimeControl = $('#events_' + idx + '__EndTime');

        var day = {
            Id: idControl.val(),
            ClubArea: { Id: clubAreaIdControl.val() },
            Location: locationControl.val() ? locationControl.val() : '',
            Description: descriptionControl.val() ? descriptionControl.val() : '',
            EventDate: eventDateControl.val(),
            StartTime: startTimeControl.val(),
            EndTime: endTimeControl.val()
        };
        vm.diaryEvents.push(day);
    }
    return vm;
}
