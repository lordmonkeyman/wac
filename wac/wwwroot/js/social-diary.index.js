﻿const uri = 'api/SocialDiaryApi';
const requestVerificationToken = $('input[name=__RequestVerificationToken]').val();

function ConfirmDialog(message, title, yesFunction, id) {
    // https://api.jqueryui.com/dialog/
    $('<div></div>').appendTo('body')
        .html('<div><h6 class="ui-dialog-content-custom">' + message + '</h6></div>')
        .dialog({
            classes: {},
            modal: true, title: title, zIndex: 10000, autoOpen: true,
            width: 300, resizable: false,
            buttons: {
                Yes: function () {
                    yesFunction(id);
                    $(this).dialog("close");
                },
                No: function () {
                    $(this).dialog("close");
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });
}

function deleteEvent(id) {
    $.ajax({
        type: 'POST',
        url: uri + '/DeleteEvent',
        data: { __RequestVerificationToken: requestVerificationToken, id },
        success: function (data) {
            var event = $('#aside-event-' + id);
            event.hide('slow', function () {
                event.remove();
            });
        }
    });
}