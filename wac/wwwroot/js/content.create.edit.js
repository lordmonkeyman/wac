﻿$(document).ready(function () {
    $(".datepicker").each(function () {
        $(this).datepicker(
            {
                dateFormat: "DD, d MM yy"
            }
        ).on("change", function () {
            setPublishDateTime();
        });
    });
});

function validateAndBind(e) {
    $(".flash-message").remove();
    var selected = $('#ClubArea_Id option:selected').val();
    if (selected >= 1) {
        $("#ClubArea_Id").val(selected);
        setPublishDateTime();
        return true;
    } else {
        var msg = '<div class="flash-message flash-message-warning">The following field(s) are required: [Club area].</div>';
        $("section#content").prepend(msg);
        return false;
    }
}

function setPublishDateTime() {
    var selectedDate = $(".datepicker").datepicker({ dateFormat: 'dd,MM,yyyy' }).val();
    $("#PublishDateTime").val(selectedDate);
}