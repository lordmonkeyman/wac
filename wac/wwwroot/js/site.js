﻿function ConfirmDialog(message, title, yesFunction, id) {
    // https://api.jqueryui.com/dialog/
    $('<div></div>').appendTo('body')
        .html('<div><h6 class="ui-dialog-content-custom">' + message + '</h6></div>')
        .dialog({
            classes: {},
            modal: true, title: title, zIndex: 10000, autoOpen: true,
            width: 300, resizable: false,
            buttons: {
                Yes: function () {
                    yesFunction(id);
                    $(this).dialog("close");
                },
                No: function () {
                    $(this).dialog("close");
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });
}

function bufferNumberString(numberString) {
    if (numberString.length > 1) return numberString;
    return '0' + numberString;
}

function formatDateTimeToTime(value) {
    const dt = new Date(value);
    return bufferNumberString(dt.getHours().toString()) + ":" + bufferNumberString(dt.getMinutes().toString()) + ":00";
}