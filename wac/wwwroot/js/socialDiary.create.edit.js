﻿$(document).ready(function () {
    $(".datepicker").each(function () {
        $(this).datepicker(
            {
                dateFormat: "DD, d MM yy"
            }
        ).on("change", function () {
            setDateTime();
        });
    });
});

function validateAndBind(e) {
    $(".flash-message").remove();

    var requiredErrors = [];
    var title = $("#Title");
    if (title.val() === '') { requiredErrors.push('Title'); }

    if (requiredErrors.length === 0) {
        setDateTime();
        return true;
    } else {
        var msg = '<div class="flash-message flash-message-warning">The following field(s) are required: ' + requiredErrors.toString() + '.</div>';
        $("section#content").prepend(msg);
        return false;
    }
}

function setDateTime() {
    var selectedDate = $(".datepicker").datepicker({ dateFormat: 'dd,MM,yyyy' }).val();
    $("#EventDate").val(selectedDate);
}