﻿### Weston Prom Run

The Weston Prom Runs are an annual series of 9 five-mile races covering two laps of the famous Weston-super-Mare promenade. Races take place on the third Thursday of each month from the Pavillion Bar (on Weston Sea Front near Knightstone Island). Start time is 7:30pm. Runners can enter the complete 9 race series and/or individual races. Entries fees are just £15.00 for the whole 9 race series or £3.00 for a single race (please add an extra £2.00 if not ARC affiliated).  
All standards of runner take part - so don't be put off if you are a new runner.  

Competitors please note that individual points are worked out from the best 6 of the first 8 races. April is the last race that will count for individual points. The 9th race (May) is excluded so that we can get the prizes out quicker on the final night. However the Team prizes are worked out from all 9 races.  

#### Team Results  
Category Teams (i.e. Vet45 etc...) must only contain runners who fall into that category at the start of the series. All mixed category teams will be placed in the senior men/ladies team category.  

[Click Here](http://www.westonac.co.uk/docs/prom/PROM_TEAM_RESULTS_2010-2011.xls) to view the latest 2010-2011 Prom Team results (Excel Format).>  
[Click Here](http://www.westonac.co.uk/docs/prom/PROM_TEAM_RESULTS_2010-2011_SUMMARY.PDF) to view the latest 2010-2011 Prom Team Summary (PDF Format).  

Please be advised that the postal code for the Race HQ on the pre-printed application forms is incorrect. The downloadable entry for available on this site has been corrected. The correct address for the Race HQ is at the Pavilion Bar, 2 Upper Church Road, Weston-super-Mare, BS23 2DT.  

#### New for 2009/2010  
**Please Note:** Following feedback from the previous series we have amended the race number system for 2010/11\. **All competitors who enter the series will be issued one number for the whole series**. Runners must <u>keep</u> their number and re-use it in all races in the series. Runners who enter on the night still must go to Race HQ prior to the race start to collect their race number. Numbers will be posted to all runners prior to the race, late applications can collect theirs from the race HQ.