﻿### Beacon Batch

#### **NEWS**
This years race will take place on Tuesday 19 June at 7:30pm and [entries](https://www.fullonsport.com/event/beacon-batch-2018/profile) are now open. The closing date for entries is 15 June. A [paper entry](http://www.westonac.co.uk/webadmin_docs/beacon_batch/2017/Beacon_Batch_Entry_Form_2017.pdf) form is also available.

#### **DETAILS**
Note that the race headquarters is at**The Burrington Inn, Burrington Combe, Nr Bristol, North Somerset, BS40 7AT**

#### **Registration and Number Pickup**
- From 6.15-7.15pm at race headquarters.
- Race HQ: The Burrington Inn, Burrington Combe, Nr Bristol, North Somerset, BS40 7AT.

#### **Course**
- The start is a 5min walk from race HQ.
- Five miles and 1000ft of climb to the summit of the Mendips.
- Helpful marshals and plenty of marking tape to guide you along the course.
- FRA rules dictate that no accompanying dogs are allowed on the course.

#### **Facilities**
- Water station at race finish.
- Hot and cold refreshments available at The Burrington Inn (Race HQ).
- Post race presentation.
- limited car parking so please car share where possible.
- We regret that there are no changing or shower facilities available.

#### **Prizes**
- Categories: Male & Female Senior, U23, V40, V50 and V60.
- Only one prize per runner.
- Team: Male & Female (first 3 runners counting on accumulated time).

#### **Entries**
- **£6.00** in advance and**£7.00** on the day.
- Entry on the day available if capacity not reached beforehand.
- Race numbers to be picked up from race headquarters on race day.
- Cashed cheque or confirmation email indicates accepted entry.
- Postal entry closing date: Saturday 16 June 2018, or earlier if full.
- Entry limit of 150 so enter as soon as possible to guarantee a place.
- Visit http://www.westonac.co.uk for the latest information.
- Enquiries: beaconbatch@westonac.co.uk or 01934 613740.