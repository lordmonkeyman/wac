﻿## Timetrial & Handicap Runs

The 5K Handicap will be run every first Tuesday of the month.
 
During the summer months (May - September) runners should park in Morrisons car park and meet on the footpath adjacent to the petrol station from 7:00pm.
 
During the winter months (October - April) runners should meet at the Grand Pier from 7:00pm.
 
A structured warm up will be led by one of the coaching team. The first runners will start the handicap at approximately 7.10pm with the scratch handicap runners starting from approximately 7.20pm.
 
If you have not run the handicap before please see Jim Wotton who will agree a provisional start time with you using a previous 5K, 10K or prom run time if you have one. If you happen to be the first runner home using a provisional handicap you will not be announced as the winner on the night please do not be offended.
 
After each run your handicap will be re-calculated according to your performance. This will be your personal best over the last twelve months or an average if you have run 6 or more handicap races in the last 12 months.
 
The organiser’s role will be to start all runners at the correct time relevant to their handicap and record the times at the finish.
 
We expect all runners to start their stopwatches when the FIRST RUNNER STARTS. Please make sure you have taken note of your handicap so you know how long you have before your race begins. (Handicap times will be available on the night for you to check).

We will ask every runner for their time after they have crossed the finish line so that actual times can then be calculated. Times and handicaps for the following month’s race will be put on the web as soon as possible.

The Digital clock will be running for you to check your time from when the first runner starts. Please be clear when giving your time whether it is clock time or actual running time.

Any comments and/or contributions regarding the organisation of the 5K Handicap are most welcome.