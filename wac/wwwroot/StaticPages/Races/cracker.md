﻿### Christmas Cracker

#### Latest news
Congratulations to all of this years runners, you were all awesome! Check yourselves out in the official race photos which are free to download courtesy of [www.photo-fit.net](www.photo-fit.net)

[Christmas cracker 10k](https://www.photo-fit.net/v/photos/64117cgh/christmas-cracker-10k-2018)  

[Mini-cracker](https://www.photo-fit.net/v/photos/43146htk/mini-cracker-2018)  

#### THE RACE
The 2018 Christmas Cracker race will take place on Sunday December 9th. Now in its 31st year, this is Weston AC's largest race by far and people come back year after year to take part in the festive run. All standards of runner take part so don't be put off if you are a new runner. All runners are actively encouraged to wear fancy dress or at the very least one piece of tinsel.

#### COURSE
*   Measured 10k course
*   Flat, on tarmac and beach, thus not suitable for wheelchairs, sorry
*   First aid and water stations
*   Lead car, race clock, festive distance markers and marshals
*   Event risk assessment undertaken, for your safety

#### FACILITIES
*   Chip timing
*   Race HQ - at Weston College, Knightstone Road
*   Changing facilities and showers
*   Supervised kit storage (at owners risk)
*   Hot and cold refreshments available
*   Full results published on the Internet
*   Post race presentation and live entertainment

#### PRIZES
*   1st, 2nd & 3rd male and female runners
*   1st and 2nd male and female vets. V40, V45 and V50
*   1st male and female vets over 60 and 70
*   Only one prize per runner (fancy dress excepted)
*   Quality technical T-shirt and mince pie to all finishers
*   Individual and team fancy dress prizes
*   Many spot prizes

#### ENTRIES
*   Online entry is £15 (attached) and £17 (unattached) inclusive of processing fee
*   Closing date is TODAY (9 November), or earlier if the entry limit is reached before then
*   The race has closed early for the last 20 years so don't delay, enter today!
*   Minimum age is 16 years
*   Race packs will be dispatched in November
*   Check our website for the very latest race information by
*   Remember – absolutely no entries on the day so enter soon!
*   Free race photographs available for all runners to download after the race from www.photo-fit.net. Great for sharing with your friends at Christmas

#### BEGINNERS TRAINING PROGRAMME
Weston Athletic Club will be organising a 10k training programme this autumn in the run-up to the event to help runners prepare for the race. As well as preparing people physically, we’ll also give people advice on pre-race nutrition, hydration and what kit to wear when running.The Christmas Cracker training programme will begin on Monday September 24 with sessions every Monday evening at 7pm, and optional sessions every Saturday at 8am. A charge of £30 will be made at the beginning of the course and all the profits will go to Weston Hospicecare. All the coaches and leaders from the club give their time freely.Anyone wanting to sign-up or find out more should contact Jim Wotton (Tel: 01934 814208, Email: [jim.wotton@westonac.co.uk](mailto:jim.wotton@westonac.co.uk)).

#### LOCATION
Race HQ on the day is at Weston College, Knightstone Road, Weston-super-Mare, BS23 2AL. Please note that the race start is on the beach by the Grand Pier. Allow yourself 15 minutes to get from the College to the start location.

#### ENQUIRIES
For race entry enquiries contact Matthew Wheeler (Race Secretary)  
EMAIL: Cracker@WestonAC.co.uk

For race organisation enquiries contact Malcolm Gammon (Race Director)  
EMAIL: malcolmandgrace@btinternet.com  
TELEPHONE: 01934 813340