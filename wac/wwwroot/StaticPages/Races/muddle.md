﻿### Mendip Muddle

#### This years race: Sunday 14 October at 11:00am

The Mendip Muddle is our toughest race. Held over 20km (12 miles) of the finest Mendip Hills, the course is hilly (there’s 420m of climb) and set in some of the finest scenery in the West Country. There are Roman lead mines, underground rivers, a nature reserve, open moorland, potholes, an ancient rabbit warren, an Iron Age fort and prehistoric tumuli. You will run past Velvet Bottom, Rhino Rift, Beacon Batch, Twin Brooks, Dolebury, Rowberrow Bottom, Tyning's, Black Down and Rains Batch. All but 2km of the course is off road and it can get a little boggy in places so make sure you bring your off road shoes.

#### Latest News

This years race is now open for entries and will be held on on Sunday 14 October at 11:00am, at the usual location of Charterhouse Centre, Nr Blagdon, Bristol, BS40 7XR (Between Cheddar and Blagdon in North Somerset: OS grid reference 502558). Registration will be from 9:30am to 10:40am.

#### To Enter This Years Race

*   [Online entries](https://www.fullonsport.com/event/mendip-muddle-2018/profile) are now open, enter before 1 September for an early bird price of £13.
*   Printable view of the [race literature](http://www.westonac.co.uk/webadmin_docs/Muddle/2018/Mendip_Muddle_2018_Entry_Form_v1.0_Web_Version.pdf)