﻿using System;
using System.Collections.Generic;
using System.Text;
using wac.Types;

namespace wac.Interfaces.DataAccess
{
    public interface IContentDataAccess
    {
        List<Content> FetchContent(DateTime fromDate, bool activeOnly, bool featuredOnly);
        List<Content> FetchContentForClubArea(int clubAreaId, DateTime fromDate, bool activeOnly, bool featuredOnly);
        List<Content> FetchFeaturedContent();
        Content CreateContent(Content content);
        bool UpdateContent(Content content);
        bool DeleteContent(int id);
        Content GetContent(int id);
        List<Content> SearchContentTitle(bool activeOnly, string searchTerm);
        List<Content> SearchContentByDate(bool activeOnly, DateTime date);
        List<Content> SearchContentBody(bool activeOnly, string searchTerm);
    }
}
