﻿using System;
using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.DataAccess
{
    public interface ITrainingDiaryDataAccess
    {
        List<Event> Fetch(int clubAreaId, DateTime weekCommencing);
        Event Get(int eventId);
        bool Delete(int eventId);
        bool Update(Event @event);
        Types.Event Create(Event @event);
        List<Event> Fetch(DateTime weekCommencing);
    }
}
