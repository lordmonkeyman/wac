﻿using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.DataAccess
{
    public interface ISocialDiaryDataAccess
    {
        List<SocialEvent> Fetch(int clubAreaId, bool limit);
        SocialEvent Get(int id);
        bool Delete(int id);
        bool Update(SocialEvent socialEvent);
        SocialEvent Create(SocialEvent SocialEvent);
    }
}
