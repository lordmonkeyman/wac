﻿using wac.Types;
namespace wac.Interfaces.DataAccess.Emails
{
    public interface IEmailDataAccess
    {
        Email GetEmail(int id);
    }
}
