﻿using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.DataAccess
{
    public interface IMessagesDataAccess
    {
        void Create(Message message);
        List<Message> Fetch(int boardId);
    }
}
