﻿using System.Collections.Generic;
using System.Threading.Tasks;
using wac.Types;

namespace wac.Interfaces.DataAccess
{
    public interface IUploadDataAccess
    {
        List<Upload> GetUploads();
        bool DeleteUpload(int id);
        Task AddUpload(Upload upload);
        Upload GetUpload(int id);
    }
}
