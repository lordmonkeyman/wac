﻿using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.DataAccess
{
    public interface IMessageBoardsDataAccess
    {
        List<MessageBoard> Fetch();
        MessageBoard Get(int id);
    }
}
