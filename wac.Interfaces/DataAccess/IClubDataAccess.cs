﻿using System;
using System.Collections.Generic;
using System.Text;
using wac.Types;

namespace wac.Interfaces.DataAccess
{
    public interface IClubDataAccess
    {
        List<ClubArea> GetClubAreas();
    }
}
