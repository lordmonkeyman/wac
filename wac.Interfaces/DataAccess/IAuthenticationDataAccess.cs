﻿using wac.Types;

namespace wac.Interfaces.DataAccess.Access
{
    public interface IAuthenticationDataAccess
    {
        User RequestToken(string email);
        void UpdateUserWithTokenAndExpiry(User user);
        bool ValidateToken(string token, out User user);
    }
}
