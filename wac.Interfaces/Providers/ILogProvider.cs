﻿using System;
using System.Collections.Generic;
using System.Text;

namespace wac.Interfaces.Providers
{
    public interface ILogProvider
    {
        void Log(string message);
        void Clear();
    }
}
