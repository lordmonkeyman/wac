﻿using System.Collections.Generic;
using System.Threading.Tasks;
using wac.Types;

namespace wac.Interfaces.Providers
{
    public interface IUploadProvider
    {
        Task AddUpload(Upload upload);
        List<Upload> GetUploads();
        bool DeleteUpload(int id);
        Types.Upload GetUpload(int id);
    }
}
