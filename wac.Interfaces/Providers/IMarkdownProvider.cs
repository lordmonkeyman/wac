﻿namespace wac.Interfaces.Providers
{
    public interface IMarkdownProvider
    {
        string ConvertMarkdownToHtml(string markDown);
    }
}
