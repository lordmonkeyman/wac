﻿using System;

namespace wac.Interfaces.Providers
{
    public interface IDateTimeProvider
    {
        DateTime GetUtcNow();
        DateTime GetDateOnly(DateTime dateTime);
        DateTime CalculateWeekEnding(DateTime weekCommencing);
        DateTime CalculateWeekCommencing(DateTime dateTime, DayOfWeek dayOfWeek);
        string ToClientSideDate(DateTime dateTime);
    }
}
