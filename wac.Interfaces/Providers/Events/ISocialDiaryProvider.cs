﻿using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.Providers.Events
{
    public interface ISocialDiaryProvider
    {
        List<SocialEvent> FetchSocialDiary(int clubAreaId, bool limit);
        bool DeleteEvent(int id);
        SocialEvent CreateSocialEvent(SocialEvent socialEvent);
        bool UpdateSocialEvent(SocialEvent socialEvent);
        SocialEvent GetSocialEvent(int id);
    }
}
