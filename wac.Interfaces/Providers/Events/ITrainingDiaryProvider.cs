﻿using System;
using System.Collections.Generic;

namespace wac.Interfaces.Providers.Events
{
    public interface ITrainingDiaryProvider
    {
        List<Types.Event> FetchTrainingDiary(int clubAreaId, DateTime weekCommencing);
        Types.Event GetTrainingDiaryEntry(int eventId);
        bool DeleteTrainingDiaryEntry(int eventId);
        bool UpdateTrainingDiaryEntry(Types.Event @event);
        Types.Event CreateTrainingDiaryEntry(Types.Event @event);
    }
}
