﻿using System.Threading.Tasks;
using wac.Types;

namespace wac.Interfaces.Providers
{
    public interface IEmailProvider
    {
        Task SendEmailAsync(Email email);
        Email GetEmailProperties(int id);
    }
}
