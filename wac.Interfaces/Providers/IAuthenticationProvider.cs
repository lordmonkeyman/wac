﻿using System.Collections.Generic;
using System.Threading.Tasks;
using wac.Types;

namespace wac.Interfaces.Providers
{
    public interface IAuthenticationProvider
    {
        User RequestToken(string email);
        bool ValidateToken(string token, out User user);
        bool HasRole(string Role);
        bool HasRole(List<string> roles);
    }
}
