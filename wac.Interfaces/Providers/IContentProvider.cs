﻿using System;
using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.Providers
{
    public interface IContentProvider
    {
        List<Content> FetchForClubArea(int clubAreaId, DateTime fromDate, bool activeOnly, bool featuredOnly);
        List<Content> GetFeaturedContent();
        Content CreateContent(Content content);
        Content GetContent(int id);
        bool UpdateContent(Content content);
        bool DeleteContent(int id);
        List<Content> SearchContentTitle(bool activeOnly, string searchTerm);
        List<Content> SearchContentByDate(bool activeOnly, DateTime date);
        List<Content> SearchContentBody(bool activeOnly, string searchTerm);
    }
}
