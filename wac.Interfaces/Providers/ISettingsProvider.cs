﻿using wac.Types;

namespace wac.Interfaces.Providers
{
    public interface ISettingsProvider
    {
        Settings Get();
    }
}
