﻿using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.Providers
{
    public interface IMessageBoardsProvider
    {
        List<MessageBoard> GetAllMessageBoards();
        MessageBoard GetMessageBoard(int id);
    }
}
