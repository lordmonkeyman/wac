﻿using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.Providers
{
    public interface IClubProvider
    {
        List<ClubArea> GetClubAreas();
    }
}
