﻿using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.Providers
{
    public interface IMessagesProvider
    {
        void CreateNewMessage(Message message);
        List<Message> GetTopLevelMessages(int boardId);
    }
}
