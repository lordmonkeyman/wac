﻿namespace wac.Interfaces.Providers
{
    public interface IStaticPageProvider
    {   string GetContent(string folder, string page);
    }
}
