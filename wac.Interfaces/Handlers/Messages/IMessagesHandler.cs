﻿
using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.Handlers.Messages
{
    public interface IMessagesHandler
    {
        void CreateNewMessage(string title, string text, int parentId, int userId);
        List<Message> GetTopLevelMessages(int boardId);
        List<MessageBoard> GetAllMessageBoards();
        MessageBoard GetMessageBoard(int id);
    }
}
