﻿using System;
using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.Handlers.Content
{
    public interface IContentHandler
    {
        List<Types.Content> GetFeaturedContent();
        List<Types.Content> FetchContentForClubArea(int clubAreaId, DateTime fromDate, bool activeOnly, bool featuredOnly);
        List<ClubArea> GetClubAreas();
        bool HasContentAdministratorRole();
        Types.Content CreateContent(Types.Content content);
        Types.Content GetContent(int id);
        bool UpdateContent(Types.Content content);
        bool DeleteContent(int id);
        List<Types.Content> SearchContent(bool activeOnly, string searchTerm);
    }
}
