﻿using System;
using System.Collections.Generic;
using System.Text;
using wac.Types;

namespace wac.Interfaces.Handlers.Events
{
    public interface ITrainingDiaryHandler
    {
        Types.Event GetTrainingDiaryEntry(int Id);
        Types.Event CreateTrainingDiaryEvent(Types.Event trainingDiaryEvent);
        Types.Event UpdateTrainingDiaryEvent(Types.Event trainingDiaryEvent);
        List<Types.Event> FetchTrainingDiaryForClubArea(int clubAreaId, DateTime weekCommencing);
        List<Types.Event> FetchTrainingDiary(DateTime weekCommencing);
        bool HasTrainingDiaryAdministratorRole();
        DateTime WeekCommencing();
        List<Event> Save(List<Event> events);
        List<ClubArea> GetClubAreas();
    }
}
