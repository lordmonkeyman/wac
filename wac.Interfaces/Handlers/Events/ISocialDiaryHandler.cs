﻿using System.Collections.Generic;
using wac.Types;

namespace wac.Interfaces.Handlers.Events
{
    public interface ISocialDiaryHandler
    {
        List<SocialEvent> FetchSocialDiaryForClubArea(int clubAreaId, bool limit);
        bool HasSocialDiaryAdministratorRole();
        bool DeleteEvent(int id);
        SocialEvent CreateSocialEvent(SocialEvent socialEvent);
        bool UpdateSocialEvent(SocialEvent socialEvent);
        SocialEvent GetSocialEvent(int id);
    }
}
