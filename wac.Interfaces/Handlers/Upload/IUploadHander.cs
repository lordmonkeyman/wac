﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace wac.Interfaces.Handlers.Upload
{
    public interface IUploadHander
    {
        Task Upload(IFormFile file);
        List<Types.Upload> GetUploads();
        bool DeleteUpload(int id);
    }
}
