﻿namespace wac.Interfaces.Handlers.StaticPage
{
    public interface IStaticPageHandler
    {
        string GetContent(string folder, string page);
    }
}
