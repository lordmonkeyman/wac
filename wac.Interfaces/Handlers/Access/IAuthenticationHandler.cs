﻿using wac.Types;

namespace wac.Interfaces.Handlers.Access
{
    public interface IAuthenticationHandler
    {
        void RequestAuthentication(LoginRequest loginRequest);
        bool ValidateToken(string token, out User user);
    }
}
