﻿using System.IO;
using wac.Interfaces.Providers;
using Microsoft.AspNetCore.Hosting;
using wac.Types;
using Microsoft.AspNetCore.Http;
using System;

namespace wac.Providers
{
    public class StaticPageProvider : IStaticPageProvider
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IMarkdownProvider _markdownProvider;
        private readonly Settings _settings;

        public StaticPageProvider(IHostingEnvironment environment, ISettingsProvider settingsProvider, IMarkdownProvider markdownProvider)
        {
            _hostingEnvironment = environment;
            _markdownProvider = markdownProvider;
            _settings = settingsProvider.Get();
        }

        public string GetContent(string folder, string page)
        {
            string content = string.Empty;
            var path = Path.Combine(_hostingEnvironment.WebRootPath, $"StaticPages/{folder}/{page}.md");

            FileStream fs = new FileStream(path, FileMode.Open);
            using(StreamReader sr = new StreamReader(fs))
            {
                content = sr.ReadToEnd();
            }

            return _markdownProvider.ConvertMarkdownToHtml(content);
        }
    }
}
