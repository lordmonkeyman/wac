﻿using System;
using System.IO;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.Providers
{
    public class LogFileProvider : ILogProvider
    {
        private readonly Settings _settings;
        private readonly string _logPath;

        public LogFileProvider(ISettingsProvider settingsProvider)
        {
            _settings = settingsProvider.Get();
            _logPath = _settings.Logging.LogPath;
        }

        public void Clear()
        {
            if (File.Exists(_logPath))
            {
                File.Delete(_logPath);
            }
        }

        public void Log(string message)
        {
            message = DateTime.UtcNow.ToString("yyyy.MM.dd hh:mm:ss") + ": " + message;

            if (!File.Exists(_logPath))
            {
                using (StreamWriter sw = File.CreateText(_logPath))
                {
                    sw.WriteLine(message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(_logPath))
                {
                    sw.WriteLine(message);
                }
            }
        }
    }
}
