﻿using System;
using System.Collections.Generic;
using System.Text;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.Providers
{
    public class ClubProvider : IClubProvider
    {
        private readonly IClubDataAccess _clubDataAccess;

        public ClubProvider(IClubDataAccess clubDataAccess)
        {
            _clubDataAccess = clubDataAccess;
        }

        public List<ClubArea> GetClubAreas()
        {
            return _clubDataAccess.GetClubAreas();
        }
    }
}
