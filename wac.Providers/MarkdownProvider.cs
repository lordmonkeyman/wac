﻿using Markdig;
using wac.Interfaces.Providers;

namespace wac.Providers
{
    public class MarkdownProvider : IMarkdownProvider
    {
        public string ConvertMarkdownToHtml(string markDown)
        {
            var pipeline = new MarkdownPipelineBuilder()
                .UseAdvancedExtensions()
                .DisableHtml()
                .UseEmphasisExtras()
                .UseGenericAttributes()
                .UseAutoLinks()
                .UseEmojiAndSmiley()
                .Build();
            return Markdown.ToHtml(markDown, pipeline);
        }
    }
}
