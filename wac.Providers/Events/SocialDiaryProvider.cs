﻿using System.Collections.Generic;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers.Events;
using wac.Types;

namespace wac.Providers.Events
{
    public class SocialDiaryProvider : ISocialDiaryProvider
    {
        private readonly ISocialDiaryDataAccess _socialDiaryDataAccess;

        public SocialDiaryProvider(ISocialDiaryDataAccess socialDiaryDataAccess) {
            _socialDiaryDataAccess = socialDiaryDataAccess;
        }

        public SocialEvent CreateSocialEvent(SocialEvent socialEvent) => _socialDiaryDataAccess.Create(socialEvent);

        public bool DeleteEvent(int id) => _socialDiaryDataAccess.Delete(id);

        public List<SocialEvent> FetchSocialDiary(int clubAreaId, bool limit) => _socialDiaryDataAccess.Fetch(clubAreaId, limit);

        public SocialEvent GetSocialEvent(int id) => _socialDiaryDataAccess.Get(id);

        public bool UpdateSocialEvent(SocialEvent socialEvent)
        {
            return _socialDiaryDataAccess.Update(socialEvent);
        }
    }
}
