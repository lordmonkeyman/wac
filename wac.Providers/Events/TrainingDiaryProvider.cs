﻿using System;
using System.Collections.Generic;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Interfaces.Providers.Events;
using wac.Types;

namespace wac.Providers.Events
{
    public class TrainingDiaryProvider : ITrainingDiaryProvider
    {
        private readonly ITrainingDiaryDataAccess _trainingDiaryDataAccess;
        private readonly ILogProvider _logProvider;

        public TrainingDiaryProvider(ITrainingDiaryDataAccess trainingDiaryDateAccess, ILogProvider logProvider)
        {
            _trainingDiaryDataAccess = trainingDiaryDateAccess;
            this._logProvider = logProvider;
        }

        public List<Event> FetchTrainingDiary(int clubAreaId, DateTime weekCommencing)
        {
            if (clubAreaId > 0)
            {
                return _trainingDiaryDataAccess.Fetch(clubAreaId, weekCommencing);
            }
            else
            {
                return _trainingDiaryDataAccess.Fetch(weekCommencing);
            }
        }

        public Event CreateTrainingDiaryEntry(Event @event)
        {
            return _trainingDiaryDataAccess.Create(@event);
        }

        public bool DeleteTrainingDiaryEntry(int eventId)
        {
            throw new NotImplementedException();
        }

        public Event GetTrainingDiaryEntry(int eventId)
        {
            throw new NotImplementedException();
        }

        public bool UpdateTrainingDiaryEntry(Event @event)
        {
            return _trainingDiaryDataAccess.Update(@event);
        }
    }
}
