﻿using System;
using System.Collections.Generic;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.Providers
{
    public class ContentProvider : IContentProvider
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly IContentDataAccess _contentDataAccess;
        private readonly Settings _settings;

        public ContentProvider(ISettingsProvider settingsService, IContentDataAccess contentDataAccess)
        {
            _settingsProvider = settingsService;
            _settings = _settingsProvider.Get();
            _contentDataAccess = contentDataAccess;
        }

        public Content CreateContent(Content content)
        {
            return _contentDataAccess.CreateContent(content);
        }

        public bool DeleteContent(int id)
        {
            return _contentDataAccess.DeleteContent(id);
        }

        public List<Content> FetchForClubArea(int clubAreaId, DateTime fromDate, bool activeOnly, bool featuredOnly)
        {
            if(clubAreaId == 0)
            {
                return _contentDataAccess.FetchContent(fromDate, activeOnly, featuredOnly);
            }
            return _contentDataAccess.FetchContentForClubArea(clubAreaId, fromDate, activeOnly, featuredOnly);
        }

        public Content GetContent(int id)
        {
            return _contentDataAccess.GetContent(id);
        }

        public List<Content> GetFeaturedContent()
        {
            return _contentDataAccess.FetchFeaturedContent();
        }

        public List<Content> SearchContentTitle(bool activeOnly, string searchTerm)
        {
            return _contentDataAccess.SearchContentTitle(activeOnly, searchTerm);
        }

        public List<Content> SearchContentByDate(bool activeOnly, DateTime date)
        {
            return _contentDataAccess.SearchContentByDate(activeOnly, date);
        }

        public bool UpdateContent(Content content)
        {
            return _contentDataAccess.UpdateContent(content);
        }

        public List<Content> SearchContentBody(bool activeOnly, string searchTerm)
        {
            return _contentDataAccess.SearchContentBody(activeOnly, searchTerm);
        }
    }
}
