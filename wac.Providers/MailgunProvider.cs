﻿//using RestSharp;
//using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using wac.Interfaces.DataAccess.Emails;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.Providers
{
    public class MailgunProvider : IEmailProvider
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly IEmailDataAccess _emailDataAccess;
        private readonly ILogProvider _logProvider;

        public MailgunProvider(ISettingsProvider settingsService, IEmailDataAccess emailDataAccess, ILogProvider logProvider)
        {
            _settingsProvider = settingsService;
            _emailDataAccess = emailDataAccess;
            _logProvider = logProvider;
        }

        public Email GetEmailProperties(int id)
        {
            var email = _emailDataAccess.GetEmail(id);
            return email;
        }

        public async Task SendEmailAsync(Email email)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes("api" + ":" + email.ApiKey)));

            var form = new Dictionary<string, string>();
            form["from"] = email.From;
            form["to"] = email.To;
            form["subject"] = email.Subject;
            form["text"] = email.Text;

            var response = await client.PostAsync("https://api.mailgun.net/v3/" + email.DomainName + "/messages", new FormUrlEncodedContent(form));
        }
    }
}
