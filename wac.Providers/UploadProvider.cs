﻿using System.Collections.Generic;
using System.Threading.Tasks;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.Providers
{
    public class UploadProvider : IUploadProvider
    {
        private readonly IUploadDataAccess _uploadDataAccess;

        public UploadProvider(IUploadDataAccess uploadDataAccess)
        {
            _uploadDataAccess = uploadDataAccess;
        }

        public async Task AddUpload(Upload upload)
        {
            await _uploadDataAccess.AddUpload(upload);
        }

        public bool DeleteUpload(int id)
        {
            return _uploadDataAccess.DeleteUpload(id);
        }

        public Upload GetUpload(int id)
        {
            return _uploadDataAccess.GetUpload(id);
        }

        public List<Upload> GetUploads()
        {
            return _uploadDataAccess.GetUploads();
        }
    }
}
