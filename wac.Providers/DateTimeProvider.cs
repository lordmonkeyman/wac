﻿using System;
using wac.Interfaces.Providers;
using wac.Types.Constants;

namespace wac.Providers
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime CalculateWeekCommencing(DateTime dateTime, DayOfWeek startDayOfWeek)
        {
            var diff = (7 + (dateTime.DayOfWeek - startDayOfWeek)) % 7;
            return dateTime.AddDays(-1 * diff).Date;
        }

        public DateTime CalculateWeekEnding(DateTime weekCommencing)
        {
            var start = (int)weekCommencing.DayOfWeek;
            var sunday = 7;
            var date = weekCommencing.AddDays(sunday - start);

            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }

        public DateTime GetUtcNow() => DateTime.UtcNow;

        public DateTime GetDateOnly(DateTime dateTime) => new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0);

        public string ToClientSideDate(DateTime dateTime) => dateTime.ToString(Constants.CLIENTSIDE_DATETIME_FORMAT);
    }
}
