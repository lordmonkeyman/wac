﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using wac.Interfaces.DataAccess.Access;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.Providers
{
    public class AuthenticationProvider : IAuthenticationProvider
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly IAuthenticationDataAccess _authenticationDataAccess;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogProvider _logProvider;
        private readonly Settings _settings;

        public AuthenticationProvider(ISettingsProvider settingsService, IAuthenticationDataAccess authenticationDataAccess, IHttpContextAccessor httpContextAccessor, ILogProvider logProvider)
        {
            _settingsProvider = settingsService;
            _settings = _settingsProvider.Get();
            _authenticationDataAccess = authenticationDataAccess;
            _httpContextAccessor = httpContextAccessor;
            _logProvider = logProvider;
        }

        public bool HasRole(string role)
        {
            var user =  _httpContextAccessor.HttpContext.User;
            foreach (var claim in user.Claims)
            {
                if (claim.Type == ClaimTypes.Role && claim.Value == role) return true;
            }

            return false;
        }

        public bool HasRole(List<string> roles)
        {
            var user = _httpContextAccessor.HttpContext.User;
            foreach (var claim in user.Claims)
            {
                if (claim.Type == ClaimTypes.Role && roles.Contains(claim.Value)) return true;
            }

            return false;
        }

        public User RequestToken(string email)
        {
            var user = _authenticationDataAccess.RequestToken(email.ToLower());

            user.AuthenticationToken = Guid.NewGuid();
            user.AuthenticationExpiry = DateTime.Now.AddMinutes(_settings.Authorisation.ExpiryInMinutes);

            _authenticationDataAccess.UpdateUserWithTokenAndExpiry(user);

            return user;
        }

        public bool ValidateToken(string token, out User user)
        {
            return _authenticationDataAccess.ValidateToken(token, out user);
        }
    }
}
