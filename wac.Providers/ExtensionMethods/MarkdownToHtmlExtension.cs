﻿namespace wac.Providers.ExtensionMethods
{
    public static class MarkdownToHtmlExtension
    {
        public static string MarkdownToHtml(this string markdown)
        {
            var markdownProvider = new MarkdownProvider();
            return markdownProvider.ConvertMarkdownToHtml(markdown);
        }
    }
}
