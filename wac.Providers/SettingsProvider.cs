﻿using Microsoft.Extensions.Configuration;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.Providers
{
    public class SettingsProvider : ISettingsProvider
    {
        private readonly Settings _settings;

        public SettingsProvider(IConfiguration configuration)
        {
            _settings = configuration.Get<Settings>();
        }

        public Settings Get()
        {
            return _settings;
        }
    }
}
