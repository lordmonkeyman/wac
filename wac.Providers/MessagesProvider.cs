﻿using System.Collections.Generic;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.Providers
{
    public class MessagesProvider : IMessagesProvider
    {
        private readonly IMessagesDataAccess _messagesDataAccess;

        public MessagesProvider(IMessagesDataAccess messagesDataAccess)
        {
            _messagesDataAccess = messagesDataAccess;
        }

        public void CreateNewMessage(Message message)
        {
            _messagesDataAccess.Create(message);
        }
        
        public List<Message> GetTopLevelMessages(int boardId)
        {
            return _messagesDataAccess.Fetch(boardId);
        }
    }
}
