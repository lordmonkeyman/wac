﻿using System.Collections.Generic;
using wac.Interfaces.DataAccess;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.Providers
{
    public class MessageBoardsProvider : IMessageBoardsProvider
    {
        private readonly IMessageBoardsDataAccess _messageBoardsDataAccess;

        public MessageBoardsProvider(IMessageBoardsDataAccess messageBoardsDataAccess)
        {
            _messageBoardsDataAccess = messageBoardsDataAccess;
        }

        public List<MessageBoard> GetAllMessageBoards()
        {
            return _messageBoardsDataAccess.Fetch();
        }

        public MessageBoard GetMessageBoard(int id)
        {
            return _messageBoardsDataAccess.Get(id);
        }
    }
}
