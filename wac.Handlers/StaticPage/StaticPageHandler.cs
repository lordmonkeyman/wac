﻿using Microsoft.Extensions.Caching.Memory;
using System;
using wac.Interfaces.Handlers.StaticPage;
using wac.Interfaces.Providers;
using wac.Types;

namespace wac.Handlers.StaticPage
{
    public class StaticPageHandler : IStaticPageHandler
    {
        private readonly IStaticPageProvider _staticPageProvider;
        private readonly IMemoryCache _cache;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly Settings _settings;

        public StaticPageHandler(IStaticPageProvider staticPageProvider, IMemoryCache cache, ISettingsProvider settingsProvider, IDateTimeProvider dateTimeProvider)
        {
            _staticPageProvider = staticPageProvider;
            _cache = cache;
            _dateTimeProvider = dateTimeProvider;
            _settings = settingsProvider.Get();
        }

        public string GetContent(string folder, string page)
        {
            var cacheKey = $"{folder}-{page}";
            
            if (_settings.Caching.UseCaching && _cache.TryGetValue(cacheKey, out string cachedContent))
            {
                return cachedContent;
            }

            MemoryCacheEntryOptions options = new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = _dateTimeProvider.GetUtcNow().AddSeconds(_settings.Caching.Expiration),
                Priority = CacheItemPriority.Normal
            };

            string content = _staticPageProvider.GetContent(folder, page);
            if (_settings.Caching.UseCaching)
            {
                _cache.Set(cacheKey, content, options);
            }

            return content;
        }
    }
}
