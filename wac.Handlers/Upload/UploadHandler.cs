﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using wac.Interfaces.Handlers.Upload;
using wac.Interfaces.Providers;
using wac.Types;
using wac.Types.Constants;
using wac.Types.Exceptions;

namespace wac.Handlers.Upload
{
    public class UploadHandler : IUploadHander
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IUploadProvider _uploadProvider;
        private readonly Settings _settings;
        private readonly IAuthenticationProvider _authenticationProvider;

        public UploadHandler(IHostingEnvironment environment, ISettingsProvider settingsProvider, IUploadProvider uploadProvider, IAuthenticationProvider authenticationProvider)
        {
            _hostingEnvironment = environment;
            _uploadProvider = uploadProvider;
            _settings = settingsProvider.Get();
            _authenticationProvider = authenticationProvider;
        }

        public bool DeleteUpload(int id)
        {
            if (HasContentAdministratorRole())
            {
                var upload = _uploadProvider.GetUpload(id);

                var uploadsFolder = Path.Combine(_hostingEnvironment.WebRootPath, _settings.UploadDirectory);
                var filePath = Path.Combine(uploadsFolder, upload.FileName);
                File.Delete(filePath);

                return _uploadProvider.DeleteUpload(id);
            }

            throw new NotAuthorisedException(Roles.CONTENT_ADMINISTRATOR, ActionNames.DELETE_CONTENT);
        }

        public List<Types.Upload> GetUploads()
        {
            if (HasContentAdministratorRole())
            {
                var uploads = _uploadProvider.GetUploads();
                uploads.ForEach(x => x.Link = $"../{_settings.UploadDirectory}/{x.FileName}");

                return uploads;
            }

            throw new NotAuthorisedException(Roles.CONTENT_ADMINISTRATOR, ActionNames.DELETE_CONTENT);
        }

        public async Task Upload(IFormFile file)
        {
            if (HasContentAdministratorRole())
            {

                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, _settings.UploadDirectory);

                if (file.Length > 0)
                {
                    var filePath = Path.Combine(uploads, file.FileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                        await _uploadProvider.AddUpload(new Types.Upload
                        {
                            Path = filePath,
                            FileName = file.FileName
                        });
                    }
                }
            }
            else
            {
                throw new NotAuthorisedException(Roles.CONTENT_ADMINISTRATOR, ActionNames.DELETE_CONTENT);
            }
        }

        public bool HasContentAdministratorRole() => _authenticationProvider.HasRole(Roles.CONTENT_ADMINISTRATOR);
    }
}
