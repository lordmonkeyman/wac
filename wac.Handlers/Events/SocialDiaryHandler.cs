﻿using System.Collections.Generic;
using wac.Interfaces.Handlers.Events;
using wac.Interfaces.Providers;
using wac.Interfaces.Providers.Events;
using wac.Types;
using wac.Types.Constants;

namespace wac.Handlers.Events
{
    public class SocialDiaryHandler : ISocialDiaryHandler
    {
        private readonly IAuthenticationProvider _authenticationProvider;
        private readonly ISocialDiaryProvider _socialDiaryProvider;

        public SocialDiaryHandler(IAuthenticationProvider authenticationProvider, ISocialDiaryProvider socialDiaryProvider) {
            _authenticationProvider = authenticationProvider;
            _socialDiaryProvider = socialDiaryProvider;
        }

        public SocialEvent CreateSocialEvent(SocialEvent socialEvent)
        {
            if(socialEvent.ClubArea == null)
            {
                socialEvent.ClubArea = new ClubArea { Id = 1 };
            }

            return _socialDiaryProvider.CreateSocialEvent(socialEvent);
        }

        public bool DeleteEvent(int id) => _socialDiaryProvider.DeleteEvent(id);

        public List<SocialEvent> FetchSocialDiaryForClubArea(int clubAreaId, bool limit) => _socialDiaryProvider.FetchSocialDiary(clubAreaId, limit);

        public SocialEvent GetSocialEvent(int id) => _socialDiaryProvider.GetSocialEvent(id);

        public bool HasSocialDiaryAdministratorRole() => _authenticationProvider.HasRole(Roles.SOCIAL_DIARY_ADMINISTRATOR);

        public bool UpdateSocialEvent(SocialEvent socialEvent) => _socialDiaryProvider.UpdateSocialEvent(socialEvent);
    }
}
