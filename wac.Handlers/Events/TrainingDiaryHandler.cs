﻿using System;
using System.Collections.Generic;
using wac.Interfaces.Handlers.Events;
using wac.Interfaces.Providers;
using wac.Interfaces.Providers.Events;
using wac.Types;
using wac.Types.Constants;

namespace wac.Handlers.Events
{
    public class TrainingDiaryHandler : ITrainingDiaryHandler
    {
        private readonly IAuthenticationProvider _authenticationProvider;
        private readonly ITrainingDiaryProvider _trainingDiaryProvider;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IClubProvider _clubProvider;

        public TrainingDiaryHandler(IAuthenticationProvider authenticationProvider, ITrainingDiaryProvider trainingDiaryProvider, IDateTimeProvider dateTimeProvider, IClubProvider clubProvider)
        {
            _authenticationProvider = authenticationProvider;
            _trainingDiaryProvider = trainingDiaryProvider;
            _dateTimeProvider = dateTimeProvider;
            _clubProvider = clubProvider;
        }

        public bool HasTrainingDiaryAdministratorRole() => _authenticationProvider.HasRole(Roles.TRAINING_DIARY_ADMINISTRATOR);

        public List<Event> FetchTrainingDiaryForClubArea(int clubAreaId, DateTime weekCommencing)
        {
            return BufferWeekWithEmptyDays(_trainingDiaryProvider.FetchTrainingDiary(clubAreaId, weekCommencing), clubAreaId, weekCommencing);
        }

        public List<Event> FetchTrainingDiary(DateTime weekCommencing)
        {
            return BufferWeekWithEmptyDays(_trainingDiaryProvider.FetchTrainingDiary(0, weekCommencing), 1, weekCommencing);
        }

        private List<Event> BufferWeekWithEmptyDays(List<Event> week, int clubAreaId, DateTime weekCommencing)
        {
            List<Event> fullWeek = new List<Event>();
            for (var day = 1; day < 8; day++)
            {
                var e = week.Find(x => x.DayOfWeek() == day);
                if (e != null)
                {
                    fullWeek.Add(e);
                }
                else
                {
                    fullWeek.Add(new Event {
                        EventDate = weekCommencing.AddDays(day - 1),
                        ClubArea = new ClubArea { Id = clubAreaId }
                    });
                }
            }

            return fullWeek;
        }

        public Event CreateTrainingDiaryEvent(Event trainingDiaryEvent)
        {
            throw new NotImplementedException();
        }

        public Event GetTrainingDiaryEntry(int Id)
        {
            throw new NotImplementedException();
        }

        public Event UpdateTrainingDiaryEvent(Event trainingDiaryEvent)
        {
            throw new NotImplementedException();
        }

        public DateTime WeekCommencing() => _dateTimeProvider.CalculateWeekCommencing(_dateTimeProvider.GetUtcNow(), DayOfWeek.Monday);

        public List<Event> Save(List<Event> events)
        {
            var diary = new List<Event>();

            foreach (var e in events)
            {
                if (e.Description == null) e.Description = string.Empty;
                if (e.Location == null) e.Location = string.Empty;

                if (e.Id > 0)
                {
                    _trainingDiaryProvider.UpdateTrainingDiaryEntry(e);
                    diary.Add(e);
                }
                else
                {
                    var entry = _trainingDiaryProvider.CreateTrainingDiaryEntry(e);
                    diary.Add(entry);
                }
            }

            return diary;
        }

        public List<ClubArea> GetClubAreas()
        {
                return _clubProvider.GetClubAreas();
        }
    }
}
