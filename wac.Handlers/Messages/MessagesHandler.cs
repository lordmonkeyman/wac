﻿using wac.Types;
using wac.Interfaces.Handlers.Messages;
using System;
using wac.Interfaces.Providers;
using System.Collections.Generic;

namespace wac.Handlers.Messages
{
    public class MessagesHandler : IMessagesHandler
    {
        private readonly IMessagesProvider _messagesProvider;
        private readonly IMessageBoardsProvider _messageBoardsProvider;

        public MessagesHandler(IMessagesProvider messagesProvider, IMessageBoardsProvider messageBoardsProvider)
        {
            _messagesProvider = messagesProvider;
            _messageBoardsProvider = messageBoardsProvider;
        }

        public void CreateNewMessage(string title, string text, int parentId, int userId)
        {
            Message message = new Message {
                CreatedDateTime = DateTime.Now,
                ParentId = parentId,
                Text = text,
                Title = title,
                User = new User { Id = userId }
            };
            _messagesProvider.CreateNewMessage(message);
        }

        public List<MessageBoard> GetAllMessageBoards()
        {
            return _messageBoardsProvider.GetAllMessageBoards();
        }

        public MessageBoard GetMessageBoard(int id)
        {
            var messageBoard = _messageBoardsProvider.GetMessageBoard(id);

            if (messageBoard.Id > 0)
            {
                return messageBoard;
            }

            return null;
        }

        public List<Message> GetTopLevelMessages(int boardId)
        {
            return _messagesProvider.GetTopLevelMessages(boardId);
        }
    }
}
