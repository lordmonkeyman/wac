﻿using System;
using System.Collections.Generic;
using wac.Interfaces.Handlers.Content;
using wac.Interfaces.Providers;
using wac.Types;
using wac.Types.Exceptions;
using wac.Types.Constants;

namespace wac.Handlers.Content
{
    public class ContentHandler : IContentHandler
    {
        private readonly IContentProvider _contentProvider;
        private readonly IClubProvider _clubProvider;
        private readonly ISettingsProvider _settingsProvider;
        private readonly IAuthenticationProvider _authenticationProvider;
        private readonly IMarkdownProvider _markdownProvider;

        public ContentHandler(
            IContentProvider contentProvider, 
            IClubProvider clubProvider, 
            ISettingsProvider settingsProvider, 
            IAuthenticationProvider authenticationProvider, 
            IMarkdownProvider markdownProvider
            )
        {
            _contentProvider = contentProvider;
            _clubProvider = clubProvider;
            _settingsProvider = settingsProvider;
            _authenticationProvider = authenticationProvider;
            _markdownProvider = markdownProvider;
        }

        public bool HasContentAdministratorRole() => _authenticationProvider.HasRole(Roles.CONTENT_ADMINISTRATOR);

        public List<Types.Content> FetchContentForClubArea(int clubAreaId, DateTime fromDate, bool activeOnly, bool featuredOnly)
        {
            var content = _contentProvider.FetchForClubArea(clubAreaId, fromDate, activeOnly, featuredOnly);
            content.ForEach(
                    x => x.Body = _markdownProvider.ConvertMarkdownToHtml(x.Body)
                );

            return content;
        }

        public List<ClubArea> GetClubAreas()
        {
            return _clubProvider.GetClubAreas();
        }

        public List<Types.Content> GetFeaturedContent()
        {
            var content = _contentProvider.GetFeaturedContent();
            content.ForEach(
                    x => x.Body = _markdownProvider.ConvertMarkdownToHtml(x.Body)
                );

            return content;
        }

        public Types.Content CreateContent(Types.Content content)
        {
            if (HasContentAdministratorRole())
            {
                return _contentProvider.CreateContent(content);
            }

            throw new NotAuthorisedException(Roles.CONTENT_ADMINISTRATOR, ActionNames.CREATE_CONTENT);
        }

        public bool UpdateContent(Types.Content content)
        {
            if (HasContentAdministratorRole())
            {
                var updatedContent = _contentProvider.UpdateContent(content);
                return updatedContent;
            }

            throw new NotAuthorisedException(Roles.CONTENT_ADMINISTRATOR, ActionNames.UPDATE_CONTENT);
        }

        public bool DeleteContent(int id)
        {
            if (HasContentAdministratorRole())
            {
                return _contentProvider.DeleteContent(id);
            }

            throw new NotAuthorisedException(Roles.CONTENT_ADMINISTRATOR, ActionNames.DELETE_CONTENT);
        }

        public Types.Content GetContent(int id)
        {
            var content = _contentProvider.GetContent(id);
            content.IsNew = false;

            return content;
        }

        public List<Types.Content> SearchContent(bool activeOnly, string searchTerm)
        {
            var content = new List<Types.Content>();

            if (DateTime.TryParse(searchTerm, out DateTime date))
            {
                content = _contentProvider.SearchContentByDate(activeOnly, date);
            }
            else
            {
                content = _contentProvider.SearchContentTitle(activeOnly, searchTerm);
                if (content.Count == 0)
                {
                    content = _contentProvider.SearchContentBody(activeOnly, searchTerm);
                }
            }

            content.ForEach(
                    x => x.Body = _markdownProvider.ConvertMarkdownToHtml(x.Body)
                );

            return content;
        }
    }
}
