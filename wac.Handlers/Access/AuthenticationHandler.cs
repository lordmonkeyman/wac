﻿using Microsoft.AspNetCore.Hosting;
using System;
using wac.Interfaces.Handlers.Access;
using wac.Interfaces.Providers;
using wac.Types;
using wac.Types.Constants;

namespace wac.Handlers.Access
{
    public class AuthenticationHandler : IAuthenticationHandler
    {
        private readonly ISettingsProvider _settingsProvider;
        private readonly ILogProvider _logProvider;
        private readonly IAuthenticationProvider _authenticationProvider;
        private readonly IEmailProvider _emailProvider;
        private readonly IHostingEnvironment _env;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly Settings _settings;

        public AuthenticationHandler(ISettingsProvider settingsProvider, ILogProvider logProvider, IAuthenticationProvider authenticationProvider, IEmailProvider emailProvider, IHostingEnvironment env, IDateTimeProvider dateTimeProvider)
        {
            _settingsProvider = settingsProvider;
            _logProvider = logProvider;
            _settings = _settingsProvider.Get();
            _authenticationProvider = authenticationProvider;
            _emailProvider = emailProvider;
            _env = env;
            _dateTimeProvider = dateTimeProvider;
        }

        public void RequestAuthentication(LoginRequest loginRequest)
        {
            if (string.IsNullOrEmpty(loginRequest.Password) && string.IsNullOrEmpty(loginRequest.Email))
            {
                if (DateTime.TryParseExact(loginRequest.TimeStamp, Constants.TIMESTAMP_FORMAT, null, System.Globalization.DateTimeStyles.None, out DateTime result))
                {
                    var utcNow = _dateTimeProvider.GetUtcNow();
                    if (utcNow.AddMinutes(-20) <= result)
                    {
                        var loginEmail = loginRequest.Token;
                        var user = _authenticationProvider.RequestToken(loginEmail);
                        var email = _emailProvider.GetEmailProperties(Constants.AUTHENTICATION_EMAIL_ID);
                        var linkText = $"http://{_settings.HostName}/Access/Authenticate?token={user.AuthenticationToken}";
                        var emailText = $"Please click this link to login to the WAC site {linkText}";

                        email.To = user.Email;
                        email.Text = emailText;

                        if (_env.IsDevelopment())
                        {
                            Console.WriteLine(emailText);
                        }

                        _emailProvider.SendEmailAsync(email);
                    }
                }
            }
        }

        public bool ValidateToken(string token, out User user)
        {
            return _authenticationProvider.ValidateToken(token, out user);
        }
    }
}
