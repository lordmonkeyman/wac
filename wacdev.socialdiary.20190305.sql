﻿INSERT INTO `wacstaging`.`event_type` (`id`, `description`) VALUES ('2', 'social');

CREATE TABLE `social_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_area_id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `event_date` datetime NOT NULL,
  `event_time` datetime NOT NULL,
  `location` varchar(45) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `organiser` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4

INSERT INTO `wacstaging`.`role`
(`club_area_id`,
`name`)
VALUES
(1,
'Social Diary Administrator');

INSERT INTO `wacstaging`.`user_roles` (`user_id`, `role_id`) VALUES ('1', '4');
